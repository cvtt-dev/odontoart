<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'cvtt_odontoart');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

// define('WP_HOME','http://odontoart.cvtt.com.br');
//define('WP_SITEURL','http://odontoart.cvtt.com.br');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '3&slg$lN!^k,j@=:6h=HoVO@WprOsK$`|&Y6cgm|;`d@jiD!5D.:1Ru0m?=C|H5&');
define('SECURE_AUTH_KEY',  'Y^QoR.>#hX1{t0Ed:cam;z9Xv(Z`=y7_/qadU~Z$MbK=QLJjW_b.%RnF~8,t/[S>');
define('LOGGED_IN_KEY',    '-QC!L.i0f-9suiLS+Q% 6hFs$FOJ,d8DOX/A#6fp2V !)m.6xj YyyqH,ygr?Fb3');
define('NONCE_KEY',        'S^5l{UFk6@fOf-VB{rM~%4fX-S8}C{mx]olS9B6~/6gV?OyZpyS,2=|J<(|&zkk|');
define('AUTH_SALT',        '/g` uZ4,+E6et)]ze.dCAkA|t15o,hjZU4yll*|}W~lm07AgloZ.!y1eT[4PVlzz');
define('SECURE_AUTH_SALT', 'G}/5mzTH>f9pa80[6VU{+AK~%yHnkh gGi@<A)be%%TMX&%bYW%PvY5Y={2b+HGY');
define('LOGGED_IN_SALT',   'Z|y1uV|B?(:;S[@y;.dEYx}k=@<IkH&O+v<ibO*<]*oI8?-t_n;<;<#tXU|W@spm');
define('NONCE_SALT',       '(]Xg9)5XFA]_S1$tuwX@o%K^@uvp?_pn]K&,KFf-_yxGwp?c0?!hX>Ki(Uo B]u/');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'cvtt_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');

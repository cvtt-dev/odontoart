<?php 
    /* Template Name: Página planos família */
    get_template_part('templates/html','header'); 

    global $post;
    $post_slug = $post->post_name;

    while (have_posts()) : the_post();
        $formulario = get_post_meta( get_the_ID(), 'page_capa_formulario', true);
        $temaColor = get_post_meta( get_the_ID(), 'page_template_color', true);
?>

<section class="odonto-page odonto-page--<?php echo $post_slug; ?>">
    <?php 
        include_once locate_template('templates/html-capa-form.php');
        include_once locate_template('templates/html-vantagens.php');
        include_once locate_template('templates/html-somos-especialista.php');
        include_once locate_template('templates/planos-familia/servicos.php');
        include_once locate_template('templates/html-planos.php');
        include_once locate_template('templates/html-baixe-app.php'); 
    ?>
</section>

<div id="formulario-action" class="white-popup-block mfp-hide">
    <?php echo do_shortcode($formulario); ?>
</div>

<?php endwhile; wp_reset_postdata(); ?>
<?php get_template_part('templates/html','footer'); ?>
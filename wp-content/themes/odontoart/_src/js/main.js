/*=========================================================================================
// INICIO MAIN JS
========================================================================================= */
var OdontoUI = (function() {
  function init() {
    slidePrincipal();
    animateBox();
    slideCliente();
    accordion();
    abasFilter();
    handleActiveClass(
      "[data-menu-open]",
      "[data-menu-close]",
      "[data-menu-container]"
    );
    handleActiveClass(
      "[data-side-open]",
      "[data-side-close]",
      "[data-side-container]"
    );
    subMenuToggle();
    openModalContratar();
    exibeOpcao();
    marcarConsulta();
    fullWindow();
    btnAlert();
    contactForm();
    owlRedes();
  }

  function slidePrincipal() {
    var owl = $(".odonto-slides-owl");

    var argsOwl = {
      autoplay: true,
      margin: 0,
      loop: true,
      animateOut: "fadeOut",
      animateIn: "fadeIn",
      responsiveClass: true,
      items: 1,
      mouseDrag: false,
      dots: false,
      nav: true
    };

    owl.owlCarousel(argsOwl);
  }

  function animateBox() {
    // observa scroll
    var doc = document.documentElement;
    var boxTarget = document.querySelector(".cta-especialista");
    var boxAnimate = document.querySelector(".box-retangulo");

    if (!boxAnimate) return;

    $(window).on("scroll", function() {
      var top = (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);
      var scroll = boxTarget.offsetTop;
      var activeClass = "active";

      var scrolTarget = scroll - 300;

      if (top >= scrolTarget) {
        if (!boxAnimate.classList.contains(activeClass)) {
          boxAnimate.classList.add(activeClass);
        }
      } else {
        boxAnimate.classList.remove(activeClass);
      }
    });
  }

  function slideCliente() {
    var owl = $(".odonto-clientes");

    var argsOwl = {
      autoplay: true,
      margin: 30,
      loop: true,
      items: 5,
      mouseDrag: false,
      dots: false,
      nav: true
    };

    owl.owlCarousel(argsOwl);
  }

  function accordion() {
    var allPanels = $(".odonto-perguntas .odonto-pergunta > div").hide();
    var panelTitle = $(".odonto-perguntas > .odonto-pergunta > h2");

    panelTitle.click(function() {
      allPanels.slideUp();
      panelTitle.removeClass("active");

      $(this)
        .addClass("active")
        .next()
        .slideDown();
      return false;
    });
  }

  function abasFilter() {
    var abaLink = $(".odonto-abas-filter a");
    var perguntas = $(".odonto-pergunta");

    abaLink.click(function(e) {
      e.preventDefault();

      abaLink.removeClass("active");
      $(this).addClass("active");

      // target
      var dataTarget = $(this).data("target");

      perguntas.each(function() {
        var dataPergunta = $(this).data("target");

        $(this)
          .removeClass("show")
          .addClass("hide");

        if (dataPergunta === dataTarget || dataTarget === "todos") {
          $(this)
            .removeClass("hide")
            .addClass("show");
        }
      });
    });
  }

  function handleActiveClass(btnOpen, btnClose, container) {
    var btnOpen = $(btnOpen);
    var btnClose = $(btnClose);
    var menuContainer = $(container);

    var open = function(event) {
      event.preventDefault();
      menuContainer.addClass("active");
    };

    var close = function(event) {
      event.preventDefault();
      menuContainer.removeClass("active");
    };

    btnOpen.click(open);
    btnClose.click(close);
  }

  function subMenuToggle() {
    var ctxMenu = $(".odonto-menu-mobile__item--parent");
    var btnActive = ctxMenu.find("> a");
    var subMenu = ctxMenu.find("> ul");

    var toggle = function(event) {
      event.preventDefault();
      $(this).toggleClass("active");
      return subMenu.toggleClass("active");
    };

    btnActive.click(toggle);
  }

  function openModalContratar() {
    $(".popup-modal").magnificPopup({
      type: "inline",
      preloader: false
    });
  }

  function exibeOpcao(event) {
    
    var opcao = $('#btn-consulta-show');
    
    if ($(opcao).hasClass('active')) {

      $(opcao).removeClass('active').fadeIn();
      
    } else {
      
      $(opcao).addClass('active').fadeOut();

    }

  }

  function marcarConsulta() {
    var btn = $('#btn-consulta');
    

    btn.click( function(event) {
      event.preventDefault();
      exibeOpcao(event);
    });
  }

  function exibeMenuSejaOdonto(event) {
    var lista = $('.odonto-footer ul#menu');
    
    if($(lista).hasClass('active')) {
      $(lista).removeClass('active').fadeIn();
    } else {
      $(lista).addClass('active').fadeOut();
    }
  }

  function sejaOdonto() {
    var btn = $('#btnSejaOdonto');

    btn.click( function(event) {
      event.preventDefault();
      
      exibeMenuSejaOdonto(event);
    });
  }
  
  function exibeMenuContatosImportantes(event) {
    var lista = $('#lista-contatos-importantes');

    if ($(lista).hasClass('active')) {
      $(lista).removeClass('active').fadeIn();
    } else {
      $(lista).addClass('active').fadeOut();
    }
  }

  function ContatosImportantes() {
    var btn = $('#btnContatosImportantes');

    btn.click(function (event) {
      event.preventDefault();

      exibeMenuContatosImportantes(event);
    });
  }

  function exibeMenuCentralVendas(event) {
    var lista = $('#lista-central-vendas');

    if ($(lista).hasClass('active')) {
      $(lista).removeClass('active').fadeIn();
    } else {
      $(lista).addClass('active').fadeOut();
    }
  }

  function CentralVendas() {
    var btn = $('#btnCentralVendas');

    btn.click(function (event) {
      event.preventDefault();

      exibeMenuCentralVendas(event);
    });
  }

  function exibeMenuAdminCentral(event) {
    var lista = $('#lista-admin-central');

    if ($(lista).hasClass('active')) {
      $(lista).removeClass('active').fadeIn();
    } else {
      $(lista).addClass('active').fadeOut();
    }
  }

  function adminCentral() {
    var btn = $('#btnAdmCentral');

    btn.click(function (event) {
      event.preventDefault();

      exibeMenuAdminCentral(event);
    });
  }

  function fullWindow() {

    var myWindow     = window;
    var larguraTela  = myWindow.innerWidth;
    
    if ( larguraTela <= 768 ) {
      exibeMenuSejaOdonto();
      sejaOdonto();

      exibeMenuContatosImportantes();
      ContatosImportantes(); 

      exibeMenuCentralVendas();
      CentralVendas();

      exibeMenuAdminCentral();
      adminCentral();

     
    }
    
  
    

  }
 
  function contactForm() {
    $("#fone").mask("(99)9999-9999?9");
  }
 

  function btnAlert() {

    $('#close').click(function (e) { 
      e.preventDefault();
      
      $('#alertApp').hide();
    });
  }
 

  function owlRedes() {

    
    var owl = $(".odonto-redes-galeria");

    var argsOwl = {
      autoplay: true,
      margin: 0,
      loop: true,
      items: 1,
      mouseDrag: true,
      dots: false,
      nav: true
    };

    owl.owlCarousel(argsOwl);

    /*=========================================================================================
      SET POPUP - GALERIA DESTAQUE
    =========================================================================================*/
    $('.odonto-redes-galeria').each(function () {
      $(this).magnificPopup({
        delegate: 'a', // the selector for gallery item
        type: 'image',
        tLoading: 'Carregando imagem #%curr%...',
        closeOnContentClick: false,
        closeBtnInside: false,
        mainClass: 'mfp-with-zoom mfp-img-mobile',

        image: {
          verticalFit: true,
          titleSrc: function (item) {
            return item.el.attr('title');
          }
        },

        gallery: {
          enabled: true,
          tCounter: '<span class="mfp-counter"></span>' // markup of counter
        },
        zoom: {
          enabled: true,
          duration: 300, // don't foget to change the duration also in CSS
          opener: function (element) {
            return element.find('img');
          }
        }

      });
    });

  }

 


  return {
    init: init
  };
})(jQuery);

OdontoUI.init();

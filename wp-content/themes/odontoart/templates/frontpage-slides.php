<section class="odonto-frontpage-slides">   
    <div class="container">
        <div class="odonto-slides-owl">
            <?php
                $slides = new WP_Query(array('post_type' => 'slides', 'showposts' => 3, 'orderby' => 'date'));

                while ($slides->have_posts()) : $slides->the_post(); 
                    $urlImg     = get_post_meta( get_the_ID(), 'icone_slide', true);    
                    $sutTitle   = get_post_meta( get_the_ID(), 'slide_subtitle', true);                    
                    $urlLink    = get_post_meta( get_the_ID(), 'slide_url', true);
                    $textLink   = get_post_meta( get_the_ID(), 'slide_tit_btn', true);
                ?>
                
                <article class="odonto-slides">
                    <figure class="odonto-slides__figure">
                        <img src="<?php echo wp_get_attachment_url($urlImg); ?>" class="odonto-slides__img" />
                    </figure>
                    <h2 class="odonto-slides__subtitle"><?php echo $sutTitle; ?></h2>
                    <h2 class="odonto-slides__title"><?php the_title(); ?></h2>
                    <a 
                        href="<?php echo $urlLink; ?>" 
                        class="odonto-btn odonto-btn--seta-green odonto-btn--gray"
                    >
                        <?php echo $textLink; ?>
                    </a>
                </article>

            <?php endwhile; wp_reset_query();?>
        </div>
    </div>
</section>
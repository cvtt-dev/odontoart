<ul class="odonto-list-icons">
    <li class="odonto-list-icons__item">
        <a href="<?php sLinks('facebook'); ?>" target="_blank" class="odonto-list-icons__link">
            <img src="<?php echo getImage('icones/icon-facebook.png'); ?>" />
        </a>
    </li>
    <li class="odonto-list-icons__item">
        <a href="<?php sLinks('instagram'); ?>" target="_blank" class="odonto-list-icons__link">
            <img src="<?php echo getImage('icones/icon-instagram.png'); ?>" />
        </a>
    </li>
    <li class="odonto-list-icons__item">
        <a href="<?php sLinks('youtube'); ?>" target="_blank" class="odonto-list-icons__link">
            <img src="<?php echo getImage('icones/icon-youtube.png'); ?>" />
        </a>
    </li>
    <li class="odonto-list-icons__item">
        <a href="<?php sLinks('linkedin'); ?>" target="_blank" class="odonto-list-icons__link">
            <img src="<?php echo getImage('icones/icon-linkedin.png'); ?>" />
        </a>
    </li>
</ul>
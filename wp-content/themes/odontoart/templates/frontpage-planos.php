<?php 
    $idPageEmpresa = 26;
    $idPagePlanoVoce = 29;

    $arrPages = array(
        array('id' => $idPageEmpresa, 'color' => 'purple'),        
        array('id' => $idPagePlanoVoce, 'color' => 'orange')        
    );
?>

<section class="odonto-links">
    <div class="container odonto-bg-lines">
        <div class="row scroll-mobile">
            <?php 
                foreach ( $arrPages as $idPage ) : 
                    $page       = get_post($idPage['id']);
                    $titulo     = $page->post_title;
                    $imageID    = get_post_meta( $page->ID, 'page_miniatura_image', true);
                    $urlImage   = wp_get_attachment_url( $imageID );
                    $urlPage    = get_page_link( $page->ID );
            ?>
            <div class="col-10 col-md-6">
                <article class="box-icon box-icon--<?php echo $idPage['color']?>">
                    <figure class="box-icon__figure">
                        <img src="<?php echo $urlImage; ?>" class="box-icon__img" />
                    </figure>

                    <div class="box-icon__infos">
                        <h3 class="box-icon__tit"><?php echo $titulo; ?></h3>
                        <a href="<?php echo $urlPage; ?>" class="box-icon__link">Saiba mais ></a>
                    </div>
                </article>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>
<?php if(!is_paged()): ?>
<section class="odonto-sections odonto-page-blog odonto-page-blog--destaques">
    <header class="odonto-headline">
        <h3 class="odonto-headline__title">Em Destaque</h3>
        <span class="odonto-headline__line"></span>
    </header>
    <div class="container">
        <div class="odonto-blog-wrap row">
            <?php if(wp_is_mobile()): $qtd = 2; else: $qtd = 6; endif;
            $blog = new WP_Query(array('post_type' => 'post', 'showposts' => $qtd, 'meta_key' => 'blog_destaque', 'orderby' => 'date')); ?>
            <?php //loop
            while ($blog->have_posts()) : $blog->the_post();
                $exclude[] .= get_the_ID(); ?>
                <div class="col-md-3">
                    <?php get_template_part('templates/blog','card'); ?>
                </div>
            <?php endwhile; wp_reset_query(); ?>
        </div>
    </div>
</section>
<?php endif; ?>
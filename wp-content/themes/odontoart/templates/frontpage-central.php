<section class="odonto-section odonto-section--central">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="odonto-box-central">
          <div>
            <h2 class="odonto-title odonto-title--color-orange">Central de relacionamento</h2>
            <div class="odonto-text">Sugestões, dúvidas ou reclamações? Mande uma mensagem pra gente que iremos te ajudar. Entre em contato conosco através do formulário:</div> 
          </div>

          <img src="<?php echo getImage('/img-central.png'); ?>" />
        </div>
      </div>

      <div class="col-md-6">
        <?php echo do_shortcode('[contact-form-7 id="271" title="PAGE - Central de atendimento"]'); ?>
      </div>
    </div>

  </div>
</section>
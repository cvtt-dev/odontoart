
<?php 
  $imageId = get_post_meta( get_the_ID(), 'page_image_home', true);
  $imageIconeId = get_post_meta( get_the_ID(), 'page_image_icone', true);
  $imageUrl = wp_get_attachment_image_src($imageId, 'full');
  $tituloFirst = get_post_meta( get_the_ID(), 'page_titulo_primeira', true); 
  $tituloSecond = get_post_meta( get_the_ID(), 'page_titulo_segunda', true);
  $descricao = get_post_meta( get_the_ID(), 'page_mini_descricao', true);
?>

<article class="odonto-article cta-especialista cta-especialista--small">
  <div class="container">
    <div class="row">

      <div class="col-md-6">
        <h2 class="odonto-title odonto-title--color-<?php echo $temaColor; ?>">
          <?php echo $tituloFirst; ?> <strong><?php echo $tituloSecond; ?></strong>
        </h2>
        <div class="odonto-text"><?php echo wpautop($descricao); ?></div>
        <a href="#formulario-action" class="popup-modal odonto-btn odonto-btn--seta-white odonto-btn--<?php echo $temaColor; ?>">Entrar em contato</a>
      </div>

      <div class="col-md-6">
        <div class="box-retangulo box-retangulo--<?php echo $temaColor; ?> active">
          <img class="img-maks" src="<?php echo $imageUrl[0]; ?>" width="<?php echo $imageUrl[1]; ?>" height="<?php echo $imageUrl[2]; ?>" /> 
          
          <?php if ($imageIconeId) : ?>
          <img src="<?php echo getImage('sobre-nos/icon-dente.png');?>" class="pos icone"/>
          <?php endif; ?>

         
        </div>
      </div>
      
    </div>
  </div>
</article>

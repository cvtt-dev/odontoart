<?php 
  $titFirst = get_post_meta( get_the_ID(), 'page_cabecalho_tit_primeira', true); 
  $titSecond = get_post_meta( get_the_ID(), 'page_cabecalho_tit_segunda', true);
?>
<article class="odonto-article odonto-article--planos-odontologicos">
  <div class="container">
    <h2 class="odonto-title odonto-title--color-green">
      <?php echo $titFirst; ?> <strong><?php echo $titSecond; ?></strong>
    </h2>

    <div class="odonto-text odonto-text--two-column">
      <?php the_content(); ?>
    </div>

    <div class="btns-group">
      <a href="<?php echo get_page_link(29); ?>" class="odonto-btn odonto-btn--seta-orange odonto-btn--gray">Plano para você</a>
      <a href="<?php echo get_page_link(26); ?>" class="odonto-btn odonto-btn--seta-purple odonto-btn--gray">Planos para sua empresa</a>
    </div>
  </div>
</article>
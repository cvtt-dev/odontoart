<header class="odonto-capa">
    <div class="container">
        <h2 class="odonto-capa__title"><?php the_title(); ?></h2>
    </div>

    <figure class="odonto-capa__thumbnail">
        <?php the_post_thumbnail('full'); ?>
    </figure>
</header>
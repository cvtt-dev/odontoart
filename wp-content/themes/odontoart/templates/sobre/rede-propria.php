<?php 
  $titRedeFirst = get_post_meta( get_the_ID(), 'page_rede_propria_tit_primeira', true); 
  $titRedeSecond = get_post_meta( get_the_ID(), 'page_rede_propria_tit_segunda', true);

?>
<article class="odonto-article odonto-article--rede-propria" id="redes">
  <div class="container">
    <h2 class="odonto-title odonto-title--color-black">
      <?php echo $titRedeFirst; ?> <strong><?php echo $titRedeSecond; ?></strong>
    </h2>

    <ul class="row cards-rede">
      

        <!-- rede propria -->
        <?php $slides = new WP_Query(array('post_type' => 'redes', 'showposts' => -1, 'orderby' => 'date'));

          while ($slides->have_posts()) : $slides->the_post(); 
          $redes_galeria     = get_post_meta( get_the_ID(), 'redes_galeria', false);    
          $redes_referencia  = get_post_meta( get_the_ID(), 'redes_referencia', true);    
          $latlong = get_post_meta(get_the_ID(), 'lat_long_emp', true);

          $str_explode = explode(",", $latlong);
          $lat         = $str_explode[0];
          $long        = $str_explode[1];
          $slug        = $post->post_name;
          $link        = "https://www.google.com.br/maps/place/".$lat.",".$long."/@".$lat.",".$long.",17z/data=!3m1!4b1!4m2!4m1!3e3";
          $link_waze   = "https://www.waze.com/livemap/directions?navigate=yes&latlng=".$lat."%2C".$long."&zoom=17";

        ?>
        
       
        <li class="col-md-3">
          <div class="odonto-galeria-redes odonto-redes-galeria">
            <?php foreach ($redes_galeria as $img) : 
                $caption = wp_get_attachment_caption( $img ); ?>

              <div class="odonto-galeria-redes__item">
                <a class="odonto-galeria-redes__link" href="<?php echo wp_get_attachment_url($img, 'odonto-galeria-redes__thumb'); ?>" title="<?php echo ($caption ? $caption : 'Odontart Planos Odontológicos'); ?>">
                  <?php echo wp_get_attachment_image($img, 'odonto-galeria-redes__thumb'); ?>
                  <legend class="legend">
                    <p class="desc"><?php echo $caption; ?></p>
                  </legend>
                </a>
              </div>
            <?php endforeach; ?> 
          </div>
          <address class="odonto-galeria-address">
            <h2 class="odonto-galeria-address__title"><?php echo get_the_title(); ?></h2>
            <strong class="odonto-galeria-address__desc"><?php echo get_the_content(); ?></strong>
            <?php if ( $redes_referencia ) : ?>
            <span class="odonto-galeria-address__small">
              <?php echo $redes_referencia; ?>
            </span>
            <?php endif; ?>
            
            <?php if ( $latlong ) : ?>
            <div class="odonto-galeria-redes__options">
              <a href="<?php echo $link;?>" target="_blank" class="btn">Navegar</a>
              <a href="<?php echo $link_waze;?>" target="_blank" class="btn btn--waze">Abrir no Waze</a>
            </div>
            <?php endif; ?>
          </address>

        </li>

        <?php endwhile; wp_reset_postdata();?>
     
    </ul>

  </div>
</article>
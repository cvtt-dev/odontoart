<?php 
    $idPageEmpresa = 26;
    $idPagePlanoVoce = 29;

    $arrPages = array(
        array('id' => $idPageEmpresa, 'color' => 'purple'),        
        array('id' => $idPagePlanoVoce, 'color' => 'orange')        
    );

    $settings  = get_option('options_gerais');
    $linkDentista = $settings['links_imp_dentista'];
?>

<article class="odonto-article odonto-article--perfeito">
  <div class="container">
    <h2 class="odonto-title odonto-title--color-white odonto-title--center">Perfeito para você</h2>

    <div class="row scroll-mobile">
        <?php 
            foreach ( $arrPages as $idPage ) : 
                $page       = get_post($idPage['id']);
                $titulo     = $page->post_title;
                $imageID    = get_post_meta( $page->ID, 'page_miniatura_image', true);
                $urlImage   = wp_get_attachment_url( $imageID );
                $urlPage    = get_page_link( $page->ID );
        ?>
        <div class="col-11 col-md-4">
            <article class="box-icon box-icon--small box-icon--<?php echo $idPage['color']?>">
                <figure class="box-icon__figure">
                    <img src="<?php echo $urlImage; ?>" class="box-icon__img" />
                </figure>

                <div class="box-icon__infos">
                    <h3 class="box-icon__tit"><?php echo $titulo; ?></h3>
                    <a href="<?php echo $urlPage; ?>" class="box-icon__link">Saiba mais ></a>
                </div>
            </article>
        </div>
        <?php endforeach; ?>
        
        <?php if ($linkDentista) : ?>
        <div class="col-11 col-md-4">
          <article class="box-icon box-icon--small box-icon--blue">
              <figure class="box-icon__figure">
                <img src="<?php echo getImage('/icon-dentista.jpg'); ?>" />
              </figure>

              <div class="box-icon__infos">
                  <h3 class="box-icon__tit">Para você dentista</h3>
                  <a href="<?php echo $linkDentista; ?>" target="_blank" class="box-icon__link">Entar ></a>
              </div>
          </article>
        </div>
        <?php endif; ?>
        
    </div>

  </div>
</article>

<?php 

$pageRede = new WP_Query(array('post_type' => 'page', 'p' => 81 ));
  
while ($pageRede->have_posts()) : $pageRede->the_post(); 
  $titFirst = get_post_meta( get_the_ID(), 'page_cabecalho_tit_primeira', true); 
  $titSecond = get_post_meta( get_the_ID(), 'page_cabecalho_tit_segunda', true);

  $imgId = get_post_meta( get_the_ID(), 'page_cabecalho_img', true);
?>

<article class="odonto-article odonto-article--rede-credenciada">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <h2 class="odonto-title odonto-title--color-black">
          <?php echo $titFirst; ?> <strong><?php echo $titSecond; ?></strong>
        </h2>

        <div class="odonto-text">
          <?php the_content(); ?>
        </div>

        <a href="<?php the_permalink(); ?>" class="odonto-btn odonto-btn--green odonto-btn--seta-white">Ver Rede Credenciada</a>
      </div>

      <div class="col-md-6">
        <?php if ($imgId) : ?>
        <figure class="odonto-img-center">
          <img src="<?php echo wp_get_attachment_url( $imgId ); ?>" />
        </figure>
        <?php endif; ?>
      </div>
    </div>
  </div>
</article>

<?php endwhile; wp_reset_postdata(); ?>
<?php 
  $servicos = get_post_meta( get_the_ID(), 'page_servico', true);
?>
<article class="odonto-article odonto-article--servicos">
  <div class="container">
    <h2 class="odonto-title odonto-title--color-white odonto-title--center">Especialidades Odontoart</h2>

    <ul class="row">
      <?php foreach( $servicos as $servico ) : ?>
      <li class="col-md-3">
        <div class="odonto-servicos"><?php echo $servico; ?></div>
      </li>
      <?php endforeach; ?>
    </ul>

    <a href="#formulario-action" class="popup-modal odonto-btn odonto-btn--seta-black odonto-btn--white">Contratar agora</a>
  </div>
</article>
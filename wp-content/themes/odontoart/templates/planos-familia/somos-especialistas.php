<article class="odonto-article cta-especialista cta-especialista--small">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <h2 class="odonto-title odonto-title--color-orange">Somos especialistas <strong>em Planos Odontológicos</strong></h2>
        <div class="odonto-text">
          <p>
          Somos um grupo empresarial atuando há mais de 20 anos no segmento de odontologia, com um corpo de profissionais altamente capacitados e qualificados para proporcionar sempre o melhor para nossos associados. Dispomos de dezenas de consultórios próprios e credenciados, estrategicamente localizados para oferecer maior comodidade e acesso facilitado.
          </p>
        </div>

        <a href="#" class="odonto-btn odonto-btn--seta-white odonto-btn--orange">Contratar agora</a>
      </div>
      <div class="col-md-6">

        <div class="box-retangulo active">
          <img class="img-maks" src="<?php echo getImage('/familia-feliz.jpg'); ?>" class="box-animate-level box-animate-level1" /> 

          <img 
            src="<?php echo getImage('sobre-nos/icon-dente.png');?>" 
            class="pos icone"
          />

          <!-- imagem -->
          <svg class="clip-path">
            <defs>
              <clipPath id="clip-retangulos">
                <path fill-rule="evenodd" d="M242.16 16.494l148.703 148.702c21.239 21.24 21.239 55.676 0 76.915L242.16 390.814c-21.239 21.239-55.675 21.239-76.914 0L16.543 242.111c-21.239-21.239-21.239-55.675 0-76.915L165.246 16.494c21.239-21.24 55.675-21.24 76.914 0z"/>
              </clipPath>
            </defs>
          </svg>

          <svg 
            width="339" 
            height="339" 
            viewBox="0 0 407 407" 
            class="pos sombra"
          >
            <use xlink:href="<?php echo getImage('svg/retangulo.svg#retangulos'); ?>" />
          </svg>

          <svg class="mira mira--principal" width="42" height="38" viewBox="0 0 37.62 34.34">
            <use xlink:href="<?php echo getImage('svg/seta.svg#seta'); ?>" />
          </svg>
          
          <svg class="mira mira--secondario" width="25" height="24" viewBox="0 0 37.62 34.34">
            <use xlink:href="<?php echo getImage('svg/seta.svg#seta'); ?>" />
          </svg>

          <svg class="mira mira--terciario" width="24" height="22" viewBox="0 0 37.62 34.34">
            <use xlink:href="<?php echo getImage('svg/seta.svg#seta'); ?>" />
          </svg>

          <svg class="mira mira--quartenario" width="20" height="18" viewBox="0 0 37.62 34.34">
            <use xlink:href="<?php echo getImage('svg/seta.svg#seta'); ?>" />
          </svg>
        </div>
      </div>
    </div>
  </div>
</article>
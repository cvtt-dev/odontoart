<?php 
    $categoria = get_the_category(get_the_ID());
    $catcolor  = get_term_meta( $categoria[0]->term_id, 'cor_categoria', true);
?>
<div class="odonto-blog-card odonto-blog-card--horizontal">
    <div class="odonto-blog-card__figure <?php echo(has_post_thumbnail() ? '' : 'odonto-nofigure');?>">
        <a href="<?php the_permalink(); ?>">
            <?php getThumbImg('medium','blog'); ?>
        </a>
    </div>
    <div class="odonto-blog-card__detalhes">
    <div class="odonto-blog-card__data"><?php echo get_the_date(); ?></div>

        <h3 class="odonto-blog-card__title">
            <a href="<?php the_permalink(); ?>">
                <?php the_title(); ?>
            </a>
        </h3>
        <div class="odonto-blog-card__desc">
            <?php echo get_excerpt_nbtn(450); ?>
        </div>
        <a href="<?php the_permalink(); ?>" class="odonto-blog-card__btn">Leia mais</a>

    </div>
</div>

<?php
  $args = array('post_type' => 'page', 'p' => 38);
  $pageSobre = new WP_Query( $args );

  if ( $pageSobre->have_posts() ) :
    while ($pageSobre->have_posts()) : $pageSobre->the_post();
      $imageId = get_post_meta( get_the_ID(), 'page_image_home', true);
      $imageUrl = wp_get_attachment_image_src($imageId, 'full');
      
      $tituloFirst = get_post_meta( get_the_ID(), 'page_titulo_primeira', true); 
      $tituloSecond = get_post_meta( get_the_ID(), 'page_titulo_segunda', true);
      $descricaoSobre = get_post_meta( get_the_ID(), 'page_mini_descricao', true);
?>
  <section class="odonto-section odonto-section--sobre-nos cta-especialista">
    <div class="container">
      <div class="row">

        <div class="col-md-6 odonto-section--center">        
          <div class="box-retangulo box-retangulo--green">
            <img src="<?php echo $imageUrl[0]; ?>" width="<?php echo $imageUrl[1]; ?>"  height="<?php echo $imageUrl[2]; ?>" class="img-maks" />

          </div>    
        </div>

        <div class="col-md-6">
          <h2 class="odonto-title odonto-title--primary">
            <strong><?php echo $tituloFirst; ?></strong>
            <?php echo $tituloSecond; ?>
          </h2>

          <div class="odonto-text">
            <?php echo wpautop($descricaoSobre); ?>
          </div>

          <a 
            href="<?php the_permalink(); ?>" 
            class="odonto-btn odonto-btn--seta-green odonto-btn--gray">Saiba mais</a>
        </div>
      </div>
    </div>
  </section>
  <?php endwhile; wp_reset_query();?>
<?php endif; ?>
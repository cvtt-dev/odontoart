<?php 
  $clientes = get_post_meta( get_the_ID(), 'clientes', true);
?>

<article class="odonto-article odonto-article--nosso-cliente">
  <div class="container">
    <header class="odonto-titles odonto-titles--small odonto-titles--center">
      <h2 class="odonto-title">Nossos clientes</h2>
    </header>

    <div class="odonto-clientes">
      <?php
        foreach( $clientes as $cliente ) :
            $imgId = $cliente['icone'];
            $urlLink = $client['link'];
         
         if ( $urlLink ) : ?>
          <a href="<?php echo $urlLink; ?>">
            <img src="<?php echo wp_get_attachment_url( $imgId[0] ); ?>"  />
          </a>
        <?php else : ?>
          <img src="<?php echo wp_get_attachment_url( $imgId[0] ); ?>"  />
        <?php endif; 
      endforeach; ?>
    </div>

  </div>
</article>
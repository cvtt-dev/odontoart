<article class="odonto-article odonto-article--somos-especialista cta-especialista cta-especialista--small">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <h2 class="odonto-title odonto-title--color-purple">Somo especialistas <strong>em Planos Odontológicos</strong></h2>
        <div class="odonto-text">
          <p>Somos um grupo empresarial atuando há mais de 20 anos no segmento de odontologia, com um corpo de profissionais altamente capacitados e qualificados para proporcionar sempre o melhor para nossos associados. Dispomos de dezenas de consultórios próprios e credenciados, estrategicamente localizados para oferecer maior comodidade e acesso facilitado.</p>
        </div>

        <a href="#" class="odonto-btn odonto-btn--purple odonto-btn--seta-white">Contratar agora</a>
      </div>

      <div class="col-md-6">
        <!-- imagem -->
      </div>
    </div>
  </div>
</article>
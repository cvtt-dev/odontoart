<?php 
  $tituloFirst = get_post_meta( get_the_ID(), 'page_vantagens_empresa_titulo_primeira', true); 
  $tituloSecond = get_post_meta( get_the_ID(), 'page_vantagens_empresa_titulo_segunda', true);
  $descricao = get_post_meta( get_the_ID(), 'page_vantagens_empresa_descricao', true);
  $cardsVantages = get_post_meta( get_the_ID(), 'cards_vantagem_empresa', true);
?>

<article class="odonto-article odonto-article--vantagens">
  <div class="container">
    <header class="odonto-titles odonto-titles--small odonto-titles--center">
      <h2 class="odonto-title odonto-title--black odonto-title--strong-purple">
        <?php echo $tituloFirst; ?>
        <strong><?php echo $tituloSecond; ?></strong>
      </h2>

      <div class="odonto-text"><?php echo wpautop($descricao); ?></div>
    </header>

    <ul class="scroll-mobile vantagens-empresa">
      <?php 
        foreach( $cardsVantages as $card ) : 
            $imgUrl = $card['icone'];
            $titulo = $card['titulo'];
      ?>
      <li>        
        <div class="card-icon card-icon--small grid-row-2">
          <figure class="card-icon__figure">
            <img src="<?php echo wp_get_attachment_url( $imgUrl[0] ); ?>" />
          </figure>

          <strong class="card-icon__title"><?php echo $titulo; ?></strong>
        </div>
      </li>
      <?php endforeach; ?>
    </ul>
  </div>
</article>
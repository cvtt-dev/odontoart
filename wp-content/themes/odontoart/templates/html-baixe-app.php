<?php 
  $temaColor = $temaColor ? $temaColor : 'green';
  $settings  = get_option('options_gerais');
  $titulo = $settings['app_titulo'];
  $descricao = $settings['app_descricao'];
  $lojas = $settings['app_group_images'];
  $linkLoja = $settings['app_url'];
?>

<section class="odonto-section odonto-section--app" id="apps">
  <div class="container">
    <div class="row">      
      <div class="col-md-6">

        <div class="odonto-box-central odonto-box-central--small">
            <img src="<?php echo getImage('/icon-mobile.png'); ?>" />
            <h2 class="odonto-title odonto-title--color-<?php echo $temaColor; ?>"><?php echo $titulo; ?></h2>
            <div class="odonto-text"><?php echo wpautop($descricao); ?></div> 

            <div class="imgs-apps">
              <?php foreach($lojas as $loja) : 
                $urlImage = wp_get_attachment_url( $loja['image'][0] );
                $link = $loja['link'];
              ?>
              <a href="<?php echo $link; ?>" target="_blank">
                <img src="<?php echo $urlImage; ?>" />
              </a>
              <?php endforeach; ?>
            </div>
        </div>

      </div>

      <div class="col-md-6">
        
        <div class="odonto-box-celular odonto-box-celular--<?php echo $temaColor; ?>">
          <img class="" src="<?php echo getImage('/img-app.png'); ?>" />
        </div>

      </div>
    </div>
  </div>
</section>
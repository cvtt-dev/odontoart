</main>

    <?php $settings  = get_option('options_gerais');  ?>
	<footer class="odonto-footer">
        <div class="odonto-footer__infos">

            <div class="container">

                <div class="odonto-bloco-contatos">
                   
                    <div class="odonto-bloco-contatos__item">

                        <h6 class="odonto-footer__tit" id="btnSejaOdonto">Seja Odontoart</h6>

                        <?php bem_menu('menu_3', 'odonto-footer-lista-contato'); ?>
                    </div>
                    

                    <div class="odonto-bloco-contatos__item">

                        
                        <h6 class="odonto-footer__tit" id="btnContatosImportantes">Contatos Importantes</h6>
                        
                            
                        <ul id="lista-contatos-importantes" class="odonto-footer-lista-contato">

                            <?php 
                                $contatoImportantes = $settings['group_telefone_contatos'];
                                //$contatoImportesColunas = array_chunk($contatoImportantes, 4);
                                //$count = count($contatoImportantes);
                            ?>
                        
                            
                            
                            <?php foreach($contatoImportantes as $contato) : ?>
                                <li class="odonto-footer-lista-contato__item">
                                    <h4 class="title"><?php echo $contato['titulo']; ?></h4>
                                    <div class="fone"><?php echo $contato['telefone']; ?></div>
                                </li>
                            <?php endforeach; ?>
                        </ul>
   
                     
                    </div>


                    
                    <div class="odonto-bloco-contatos__item">
                        
                                
                        <h6 class="odonto-footer__tit" id="btnCentralVendas">Central de Vendas</h6>
                            
                        <ul id="lista-central-vendas" class="odonto-footer-lista-contato">
                            <?php  
                                $centralTelefones   = $settings['group_telefone_vendas'];
                            ?>
                            <?php foreach($centralTelefones as $contato) : ?>
                                <li class="odonto-footer-lista-contato__item">
                                    <h4 class="title"><?php echo $contato['titulo']; ?></h4>
                                    <div class="fone"><?php echo $contato['telefone']; ?></div>
                                </li>
                            <?php endforeach; ?>

                        </ul>
                        
                    </div>

                    
                    <div class="odonto-bloco-contatos__item">  

                        <h6 class="odonto-footer__tit" id="btnAdmCentral">Administração Central</h6>
                        
                        <ul id="lista-admin-central" class="odonto-footer-lista-contato">

                            <?php $endereco = $settings['contato_endereco']; ?> 
                            <address class="odonto-footer__end">
                                <?php echo wpautop($endereco); ?>
                                
                                <strong class="odonto-footer__email"><?php $email = $settings['contato_email']; echo $email; ?></strong>
                            </address>
                            
                        </ul>

                        <?php get_template_part('templates/html','social'); ?>

                    </div>

                </div>
            </div>
        </div>
            
        <div class="odonto-footer__cred">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 odonto-footer__text">
                        <p><?php $credito = $settings['credito_titulo']; echo $credito; ?></p>
                    </div>

                    <div class="col-md-4 odonto-footer__logo">
                        <a href="<?php echo get_bloginfo('url'); ?>">
                            <img src="<?php echo getImage('logo.png'); ?>" />
                        </a>
                    </div>
                    
                    <div class="col-md-4 odonto-footer__logos">
                        <?php 
                            $logos = $settings['images']; 
                            foreach ($logos as $logo) :
                        ?>                           
                            <img src="<?php echo wp_get_attachment_url( $logo ); ?>" />
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="odonto-footer__cvtt"><a href="https://www.convertte.com.br/" target="_blank" class="link"><img src="<?php echo getImage('logo-cvtt.png'); ?>" alt=""></a></div> 
        
       
	</footer>
	<?php wp_footer();?>
</body>
</html>

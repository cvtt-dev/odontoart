<?php while (have_posts()) : the_post(); 
    $categoria = get_the_category(get_the_ID());
    $catcolor  = get_term_meta( $categoria[0]->term_id, 'cor_categoria', true);  
?>

    <div class="odonto-single">
        <header class="odonto-single__header">                
            <h2 class="odonto-single__title"><?php the_title(); ?></h2>
            
            <div class="odonto-single__infodate">
                <div class="odonto-single__data"><?php echo get_the_date(); ?></div>
            </div>

            <?php get_template_part( 'templates/html', 'share'); ?>
        </header>

        <?php if(has_post_thumbnail()): ?>
            <figure class="odonto-single__figure">
                <?php the_post_thumbnail('medium_large'); ?>
            </figure>
        <?php endif; ?>

        <article class="odonto-single__conteudo">
            <?php the_content(); ?>
        </article>

        <div class="odonto-single__comentarios">
            <header class="odonto-headline">
                <h3 class="odonto-headline__title">Deixe seu comentário</h3>
                <span class="odonto-headline__line"></span>
            </header>

            <div class="fb-comments" data-href="<?php echo 'http:'.get_permalink(); ?>" data-width="100%" data-numposts="5"></div>
        </div>
    </div>
    
<?php endwhile; wp_reset_postdata(); ?>

<aside class="odonto-page-single__side">

    <header class="odonto-headline">
        <h3 class="odonto-headline__title">Categorias</h3>
        <span class="odonto-headline__line"></span>
    </header>

    <ul class="odonto-list-cat">
        <?php //PEGA LISTA DE CATEGORIAS
            $args = array('orderby' => 'name', 'order' => 'ASC', 'hide_empty' => true);
            $categorias = get_categories($args);

            if (!empty($categorias) && !is_wp_error($categorias)) {
                foreach ( $categorias as $categoria ) {
                    echo "<li class='odonto-list-cat__item'>";
                    echo "<a href='".get_category_link($categoria->term_id)."' class='odonto-list-cat__link'>" . $categoria->name . "</a>";
                    echo "</li>";
                }
            }
        ?>
    </ul>

    <header class="odonto-headline">
        <h3 class="odonto-headline__title">Em Destaque</h3>
        <span class="odonto-headline__line"></span>
    </header>
    <div class="odonto-blog-wrap odonto-blog-wrap--side">
        <?php $blog = new WP_Query(array('post_type' => 'post', 'showposts' => 4, 'meta_key' => 'blog_destaque', 'post__not_in' => array(get_the_ID()), 'orderby' => 'date')); ?>
        <?php //loop
        while ($blog->have_posts()) : $blog->the_post();
            get_template_part('templates/blog-card','small');
        endwhile; wp_reset_query(); ?>
    </div>
</aside>
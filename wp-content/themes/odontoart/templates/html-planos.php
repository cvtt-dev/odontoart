<?php  $settings  = get_option('options_gerais');  ?>

<article class="odonto-article odonto-article--planos">
  <div class="container">
    <h2 class="odonto-title odonto-title--color-black odonto-title--center">Conheça nossos planos</h2>
    
    <ul class="row planos scroll-mobile">
      <?php 
        $i=0;
        $cardsNames = array('terciary', 'primary', 'secondary');
        $colors = array('blue', 'green', 'orange');
        $planos = $settings['planos_group'];

        foreach($planos as $plano) :
          $titulo = $plano['titulo'];
          $subtitulo = $plano['subtitulo'];
          $vantagens = $plano['text'];
          $observacao = $plano['obs'];
      ?>
      <li class="col-11 col-md-4">
        <div class="odonto-plano odonto-plano--<?php echo $cardsNames[$i]; ?>">
          <div class="odonto-plano__header">
            <div class="odonto-plano__titles">
              <h2><?php echo $titulo; ?></h2>
              <span><?php echo $subtitulo; ?></span>
            </div>
          </div>

          <div class="odonto-plano__content">
            <ul class="odonto-list-mark">
              <?php
              foreach($vantagens as $vantagem) {
                echo '<li>'.$vantagem.'</li>';
              }
              ?>
            </ul>
          </div>
          <div class="odonto-plano__footer">
            <a href="#formulario-action" class="popup-modal odonto-btn odonto-btn--<?php echo $colors[$i]; ?>">Contratar agora</a>
          </div>
        </div>

        <small class="odonto-obs"><?php echo $observacao; ?></small>
      </li>
      <?php $i++; endforeach; ?>

    </ul>
    
    <div class="box-center">
      <a href="<?php echo get_page_link(303); ?>" class="odonto-btn odonto-btn--text">Ver comparação completa ></a>
    </div>
  </div>
</article>
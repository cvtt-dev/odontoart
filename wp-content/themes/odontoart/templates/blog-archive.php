<div class="odonto-blog-wrap row">
    <?php
        global $wp_query;
        $merged_args = array_merge( $wp_query->query, array(
            'posts_per_page' => 6,
            'orderby'        => 'date',
            'order'          => 'DESC',
            'post__not_in'   => $exclude,
        ));
        query_posts( $merged_args );
    ?>
    <?php while (have_posts()) : the_post(); ?>
    <div class="col-md-12">
        <?php get_template_part('templates/blog','card'); ?>
    </div>
    <?php endwhile; ?>
</div>
<?php wp_pagenavi(); wp_reset_query();?>

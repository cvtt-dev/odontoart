<?php 
    get_template_part('templates/html','head'); 
    $settings  = get_option('options_gerais');  
?>
<header class="odonto-header odonto-header--desktop" data-url="<?php echo get_site_url();?>">
    <div class="container">
        <div class="odonto-header__row">
            <h1 class="odonto-header__logo">
                <a href="<?php echo get_bloginfo('url'); ?>">
                    <img src="<?php echo getImage('logo.png'); ?>" />
                </a>
            </h1>

            <div class="odonto-header__wrap">
                <div class="odonto-header__firstline">
                    <nav class="odonto-header__nav odonto-header__nav--main">
                        <?php bem_menu('menu_2', 'odonto-menu-small'); ?>
                    </nav>
                </div>

                <div class="odonto-header__secondline">
                    <menu class="odonto-header__nav odonto-header__nav--main">
                        <?php bem_menu('menu_1', 'odonto-menu'); ?>
                    </menu>

                    <address class="odonto-fone">
                        <img src="<?php echo getImage('icones/icon-fone.png'); ?>" />
                        <a href="tel:+85<?php echo $settings['contato_telefones']; ?>" class="link"><strong><?php echo $settings['contato_telefones']; ?></strong></a>
                    </address>
                    <div id="marcar-consulta" style="position: relative;">
                        <a href="#" class="odonto-btn odonto-btn--small" id="btn-consulta">
                            <img src="<?php echo getImage('icones/icon-check.png'); ?>" />
                            Marcar Consulta
                        </a>
                        <ul id="btn-consulta-show" class="btn-consulta-show">
                            <li class="btn-consulta-show__item">
                                <a href="http://corporativo.odontoartonline.com.br/SYS/?TipoUsuario=1" class="btn-consulta-show__link">Marcação de Consultas</a>
                            </li>
                            <li class="btn-consulta-show__item">
                                <a href="/passo-a-passo/" class="btn-consulta-show__link">Dúvidas? Leia aqui o Passo a Passo</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<header class="odonto-header odonto-header--mobile" data-url="<?php echo get_site_url();?>">
    <div class="container">
        <div class="odonto-header__menu" data-menu-open>
            <button class="menu-hamburguer">
                <span class="menu-hamburguer__item"></span>
                <span class="menu-hamburguer__item"></span>
                <span class="menu-hamburguer__item"></span>
            </button>
        </div>

        <h1 class="odonto-header__logo">
            <a href="<?php echo get_bloginfo('url'); ?>">
                <img src="<?php echo getImage('logo.png'); ?>" />
            </a>
        </h1>

        <ul class="odonto-header__actions">
            <li class="odonto-menu-small--sou-cliente">
                <a href="http://corporativo.odontoartonline.com.br/SYS/?TipoUsuario=1"></a>
            </li>

            <li class="odonto-menu-small--sou-dentista">
                <a href="http://corporativo.odontoartonline.com.br/SYS/?TipoUsuario=1"></a>
            </li>
        </ul>
    </div>
</header>

<div class="sheetside" data-menu-container>
    <div class="sheetside__content">
        <div class="sheetside__header">
            <a href="http://corporativo.odontoartonline.com.br/SYS/?TipoUsuario=1" class="odonto-btn odonto-btn--small" target="_blank">
                <img src="<?php echo getImage('icones/icon-check.png'); ?>" />
                Marcar Consulta
            </a>
            <a href="http://odontoart.cvtt.com.br/passo-a-passo/" class="odonto-btn odonto-btn--small" target="_blank">
                <span style="text-align:center; font-size: 13px;"> Dúvidas?<br/> Leia aqui o Passo a Passo. </span>
            </a>

        </div>


        <div class="sheetside__body">
            <nav class="odonto-header__nav">
                <?php bem_menu('menu_1', 'odonto-menu-mobile'); ?>
            </nav>

            <nav class="odonto-header__nav">
                <?php bem_menu('menu_2', 'odonto-menu-mobile'); ?>
            </nav>
        </div>
    </div>

    <a href="#" class="sheetside__close" data-menu-close>&times;</a>
    <div class="sheetside__overlay"></div>
</div>

<main class="odonto-main">

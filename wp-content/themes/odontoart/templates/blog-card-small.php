<?php //
    $categoria = get_the_category(get_the_ID());
    $catcolor  = get_term_meta( $categoria[0]->term_id, 'cor_categoria', true);
?>
<div class="odonto-blog-card odonto-blog-card--small">
    <div class="odonto-blog-card__figure <?php echo(has_post_thumbnail() ? '' : 'odonto-nofigure');?>">
        <a href="<?php the_permalink(); ?>">
            <?php geodontohumbImg('thumb-170x170','blogsmall'); ?>
        </a>
    </div>
    <div class="odonto-blog-card__detalhes">
        <a href="<?php echo get_category_link($categoria[0]->term_id);?>">
            <span class="odonto-blog-card__categoria" style="background: <?php echo $catcolor; ?>"><?php echo $categoria[0]->name; ?></span>
        </a>
        <h3 class="odonto-blog-card__title">
            <a href="<?php the_permalink(); ?>">
                <?php the_title(); ?>
            </a>
        </h3>
        <div class="odonto-blog-card__data"><?php echo get_the_date(); ?></div>
    </div>
</div>
<?php 
  global $post;
  $post_slug = $post->post_name;

  $imgId = get_post_meta( get_the_ID(), 'page_capa_image', true);
  $tituloFirst = get_post_meta( get_the_ID(), 'page_capa_titulo_primeira', true); 
  $tituloSecond = get_post_meta( get_the_ID(), 'page_capa_titulo_segunda', true);
  $descricao = get_post_meta( get_the_ID(), 'page_capa_mini_descricao', true); 
?>

<header class="odonto-capa odonto-capa--<?php echo $post_slug; ?>">
    <div class="container">
        <div class="row">
          <div class="col-md-8">

            <div class="odonto-box-cta">
              <h2 class="odonto-title odonto-title--color-white odonto-title--strong-<?php echo $temaColor; ?>"><?php echo $tituloFirst; ?> <strong><?php echo $tituloSecond; ?></strong></h2>
              <div class="odonto-text"><?php echo wpautop($descricao); ?></div>
            </div>

          </div> 

          <?php if ( $formulario ) : ?>
          <div class="col-md-4">
            <?php echo do_shortcode($formulario); ?>
          </div> 
          <?php endif; ?>
        </div>
    </div>

    <figure class="odonto-capa__thumbnail">
        <img src="<?php echo wp_get_attachment_url( $imgId ); ?>" />
    </figure>
</header>
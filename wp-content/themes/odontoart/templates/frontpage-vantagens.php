<?php 
  $pageVantagem = new WP_Query(array('post_type' => 'page', 'p' => 29));

  while ($pageVantagem->have_posts()) : $pageVantagem->the_post(); 
    $tituloFirst = get_post_meta( get_the_ID(), 'page_vantagem_titulo_primeira', true); 
    $tituloSecond = get_post_meta( get_the_ID(), 'page_vantagem_titulo_segunda', true);
    $descricao = get_post_meta( get_the_ID(), 'page_vantagem_descricao', true);
    $cardsVantages = get_post_meta( get_the_ID(), 'cards_vantagem', true);
?>

<section class="odonto-section odonto-section--vantagens">
  <div class="container grid-vantagens">
    <header class="odonto-titles">
      <h2 class="odonto-title odonto-title--secondary">
        <?php echo $tituloFirst; ?>
        <strong><?php echo $tituloSecond; ?></strong>
      </h2>
      <div class="odonto-text"><?php echo  wpautop( mb_strimwidth($descricao, 0, 140, "...")); ?></div>
    </header>

    <picture>
      <source srcset="<?php echo getImage('/bg-vantagens-mobile.png'); ?>" media="(max-width: 768px)">
      <img src="<?php echo getImage('/bg-vantagens.jpg'); ?>" />
    </picture>

    <div class="row scroll-mobile">
      <?php 
        $x=1;
        foreach( $cardsVantages as $card ) : 
            $imgUrl = $card['icone'];
            $titulo = $card['titulo'];
        if ($x == 7 ) break;
      ?>
      <div class="card-icon grid-row-<?php echo ($x > 3) ? '3' : '2'; ?>">
        <figure class="card-icon__figure">
          <img src="<?php echo wp_get_attachment_url( $imgUrl[0] ); ?>" />
        </figure>

        <strong class="card-icon__title"><?php echo $titulo; ?></strong>
      </div>
      <?php $x++; endforeach; ?>
    </div> 

    <div class="btns-group grid-row-3 grid-colunm-4">
      <a href="<?php echo get_page_link(29); ?>" class="odonto-btn odonto-btn--medium odonto-btn--seta-orange odonto-btn--gray">Plano para você</a>
      <a href="<?php echo get_page_link(26); ?>" class="odonto-btn odonto-btn--medium odonto-btn--seta-purple odonto-btn--gray">Planos para sua empresa</a>
    </div>

  </div>
</section>

<?php endwhile; wp_reset_query();?>
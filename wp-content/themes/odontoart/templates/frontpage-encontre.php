<section class="odonto-section odonto-section--encontre">
  <div class="container">

      <div class="odonto-form-center">
        <img src="<?php echo getImage('/lupa.png'); ?>" class="odonto-icon" />
        <h2 class="odonto-title">Encontre especialistas em nossa rede credenciada por todo o Brasil</h2>

        <form class="odonto-form odonto-form--outline">
          <div class="row">
            <div class="form-row col-md-6">
              <div class="odonto-select">
                <select name="estado">
                  <option>Ceará</option>
                  <option>Santa Catarina</option>
                </select>
              </div>
            </div>

            <div class="form-row col-md-6">
              <div class="odonto-select">
                <select name="cidade">
                  <option>Fortaleza</option>
                  <option>Caucaia</option>
                </select>
              </div>
            </div>
          </div>
        </form>

        <a href="<?php echo get_page_link(81); ?>" type="submit" class="odonto-btn odonto-btn--seta-green odonto-btn--gray">Ver lista completa</a>
      </div>

  </div>
</section>
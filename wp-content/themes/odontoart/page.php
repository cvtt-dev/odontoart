<?php
    get_template_part('templates/html','header'); 

    global $post;
    $post_slug = $post->post_name;

    while (have_posts()) : the_post(); 
?>

<section class="odonto-page odonto-page--<?php echo $post_slug; ?>" style="display:none;">
    <?php include_once locate_template('templates/sobre/capa.php') ; ?>
</section>

<article class="odonto-article odonto-article--contato container">
<div class="row">
<div class="col-md-10" style="margin:0 auto;">
<h2 style="text-align:center;"><?php echo the_title(); ?></h2>
<br/>
<?php echo the_content(); ?>
</div>
</div>
</article>

<?php
  endwhile; wp_reset_postdata(); 
  get_template_part('templates/html','footer'); 
?>

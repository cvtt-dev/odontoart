<?php 
    /* Template Name: Página Perguntas Frequentes */
    get_template_part('templates/html','header'); 

    global $post;
    $post_slug = $post->post_name;

    while (have_posts()) : the_post(); 
?>

<section class="odonto-page odonto-page--<?php echo $post_slug; ?>">
    <?php include_once locate_template('templates/sobre/capa.php') ; ?>

    <article class="odonto-article odonto-article--<?php echo $post_slug; ?> container">
      <div class="row">
        <div class="col-md-8">
          <h2 class="odonto-title odonto-title--color-black">Tire todas as suas <strong>dúvidas</strong></h2>

          <?php get_template_part('templates/html','info-mobile');  ?>
          <?php 
            $tipos = get_terms( 'tipos', array('hide_empty' => true));
          ?>
          <ul class="odonto-abas-filter">
            <li><a href="#" class="active" data-target="todos">Todos</a></li>
            <?php foreach( $tipos as $tipo ) : ?>
              <li>
                <a href="#" data-target="<?php echo $tipo->slug; ?>">
                  <?php echo $tipo->name; ?>
                </a>
              </li>
            <?php endforeach; ?>
          </ul>

          <div class="odonto-perguntas">
            <?php
                $perguntas = new WP_Query(array('post_type' => 'perguntas', 'showposts' => -1, 'orderby' => 'date'));
                while ($perguntas->have_posts()) : $perguntas->the_post(); 
                  $tiposSlugs = []; 
                  $tiposList = wp_get_post_terms( $post->ID, 'tipos', array( 'fields' => 'all' ) );
                  foreach($tiposList as $tipos) {
                    $tiposSlugs[] .= $tipos->slug;
                  }
            ?>
            <div data-target="<?php echo join(' ', $tiposSlugs); ?>" class="odonto-pergunta">
              <h2><?php the_title(); ?></h2>
              <div>
                <?php the_content(); ?>
              </div>
            </div>
            <?php endwhile; wp_reset_query();?>
          </div>
        </div>

        <div class="col-md-4">
          <?php get_template_part('templates/sidebar','planos'); ?>
        </div>
      </div>
    </article>
</section>

<?php
  endwhile; wp_reset_postdata(); 
  get_template_part('templates/html','footer'); 
?>
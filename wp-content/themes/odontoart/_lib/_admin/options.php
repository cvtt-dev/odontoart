<?php
/*
* Functions Opções do Tema
* Desenvolvedor: Nicholas Lima
* Email: nick.lima.wp@gmail.com
*/

//=========================================================================================
// ADICIONANDO FAVICON
//=========================================================================================

function blog_favicon() {
  echo '<link rel="Shortcut Icon" type="image/x-icon" href="'.get_template_directory_uri().'/assets/images/favicon.png" />';
}
add_action('wp_head', 'blog_favicon');

//=========================================================================================
// CONFIGURAÇÕES DO TEMA
//=========================================================================================

function tema_setup() {

  // Register wp_nav_menu() menus (http://codex.wordpress.org/Function_Reference/register_nav_menus)
  register_nav_menus(array(
    'menu_1' => 'Menu Principal',
    'menu_2' => 'Menu Secondario',
    'menu_3' => 'Menu Rodapé',
  ));

  add_editor_style('/assets/css/editor-style.css');//..Tell the TinyMCE editor to use a custom stylesheet
  add_theme_support('post-thumbnails');//..............Add post thumbnails (http://codex.wordpress.org/Post_Thumbnails)
  set_post_thumbnail_size(1200, 0, true);

  //***Switch default core markup for search form, comment form, and comments to output valid HTML5.
  //add_theme_support( 'html5', array('search-form','comment-form','comment-list','gallery','caption'));
  //***Add post formats (http://codex.wordpress.org/Post_Formats)
  //add_theme_support('post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat'));

}

add_action('after_setup_theme', 'tema_setup');
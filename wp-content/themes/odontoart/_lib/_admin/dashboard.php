<?php
/*
* Dashboard functions
* Desenvolvedor: Nicholas Lima
* Email: nick.lima.wp@gmail.com
*/

//=========================================================================================
// SAUDAÇÃO CUSTOMIZADA
//=========================================================================================

function replace_howdy( $wp_admin_bar ) {
    $my_account=$wp_admin_bar->get_node('my-account');
    $newtitle = str_replace( 'Olá', 'Bem vindo', $my_account->title );
    $wp_admin_bar->add_node( array(
        'id' => 'my-account',
        'title' => $newtitle,
    ) );
}
add_filter( 'admin_bar_menu', 'replace_howdy',25 );

//=========================================================================================
// CUSTOMIZAR O FOOTER (ASSINATURA DO PROJETO)
//=========================================================================================

function remove_footer_admin () {
    echo '© '.get_bloginfo('name');
}
add_filter('admin_footer_text', 'remove_footer_admin');

//=========================================================================================
// CSS ADMIN
//=========================================================================================

add_action( 'admin_enqueue_scripts', 'load_admin_style' );
function load_admin_style() {
    wp_register_script('jquerymask', get_template_directory_uri() . '/_lib/_admin/assets/js/jquery.mask.js', array(), '', true);
    wp_register_script('mask', get_template_directory_uri() . '/_lib/_admin/assets/js/mask.js', array(), '', true);
    wp_enqueue_style( 'admin_css', get_template_directory_uri() . '/_lib/_admin/assets/css/admin-main.css', false, '1' );

    wp_enqueue_script('jquerymask');
    wp_enqueue_script('mask');
}

// //=========================================================================================
// // SEPARATOR LABEL
// //=========================================================================================

// function wpdocs_register_my_custom_menu_page(){
//     add_menu_page('','Cursos', 'manage_options', 'dashboard_sep' ,'', 'dashicons-arrow-right-alt2', 5);
//     add_menu_page('','Fametro', 'manage_options', 'dashboard_sep' ,'', 'dashicons-arrow-right-alt2', 9);
//     add_menu_page('','Área do Aluno', 'manage_options', 'dashboard_sep' , '', 'dashicons-arrow-right-alt2', 17);
//     add_menu_page('','Opções', 'manage_options', 'dashboard_sep' ,'',  'dashicons-arrow-right-alt2', 19);
// }

// add_action( 'admin_menu', 'wpdocs_register_my_custom_menu_page' );

//=========================================================================================
// REMOVER WIDGETS
//=========================================================================================

function remove_widgets_painel() {
        remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_primary', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
        remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
        remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_activity', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
        remove_meta_box( 'wpseo-dashboard-overview', 'dashboard', 'side' );
}
add_action( 'admin_init', 'remove_widgets_painel' );

//=========================================================================================
// ADCIONANDO WIDGETS
//=========================================================================================

//ADD WIDGET SUPORTE CONVERTTE
function setup_suporte_widgets() {
    global $wp_meta_boxes;
    wp_add_dashboard_widget('custom_help_widget', 'Convertte Web Intelligence | Suporte', 'suporte_dashboard_help');
    //add_meta_box('custom_help_widget', 'Convertte Web Intelligence | Suporte', 'suporte_dashboard_help', 'dashboard', 'side', 'high');
}

function suporte_dashboard_help() {
    echo 'Se você tiver qualquer dúvida ou precisar de ajuda, por favor, entre em contato conosco em <a href="http://www.convertte.com.br" target="_blank">http://www.convertte.com.br</a> ou mande um e-mail para <a href="mailto:contato@convertte.com.br">contato@convertte.com.br</a>.</br></br>
    <h4><b>Telefone:</b> (85) 3065-7273</h4>';
}

add_action('wp_dashboard_setup', 'setup_suporte_widgets');
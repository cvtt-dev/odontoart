<?php
/*
* CForms Select Functions
* Desenvolvedor: Nicholas Lima
* Email: nick.lima.wp@gmail.com
*/
function dynamic_select_list($tag, $unused) {

    $options = (array)$tag['options'];

    foreach ($options as $option) {
        if (preg_match('%^from_db:([-0-9a-zA-Z_]+)$%', $option, $matches)) {
            $from_db = $matches[1];
        }
    }

    if(!isset($from_db)) {
        return $tag;
    }

    // get the existing WPCF7_Pipe objects
    $befores = $tag['pipes']->collect_befores();
    $afters = $tag['pipes']->collect_afters();

    // add the existing WPCF7_Pipe objects to the new pipes array
    $pipes_new = array();
    for ($i=0; $i < count($befores); $i++) {
        $pipes_new[] = $befores[$i] . '|' . $afters[$i];
    }

    //PACOTES EXCURSOES
    if($from_db === 'excursao') {
        $excursao = get_queried_object();

        if (!$excursao) {
            return $tag;
        }

        $pacotes = get_post_meta( $excursao->ID, 'excursoes_pacotes', true);
        $pacotes = array_reverse($pacotes);

        foreach ($pacotes as $item) {
            $tag['raw_values'][] = $item['desc'];
            $tag['values'][] = $item['desc'];
            $tag['labels'][] = $item['desc'];
            $pipes_new[] = $item['desc'] . '|' . $item['desc'];
        }
    }

    // setup all the WPCF7_Pipe objects
    $tag['pipes'] = new WPCF7_Pipes($pipes_new);

    return $tag;
}
add_filter('wpcf7_form_tag', 'dynamic_select_list', 10, 2);
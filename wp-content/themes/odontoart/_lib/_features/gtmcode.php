<?php
/*
* GTM Code functions
* Desenvolvedor: Nicholas Lima
* Email: nick.lima.wp@gmail.com
*/

function gtm_code($target = ''){
    $settings = get_option('options_gerais');
    echo $settings['gtm_'.$target];
}
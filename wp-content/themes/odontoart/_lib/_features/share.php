<?php
/**
* Share Functions
* Desenvolvedor: Nicholas Lima
* Email: nick.lima.wp@gmail.com
*/
//=========================================================================================
// COMPARTILHAR
//=========================================================================================

function get_compartilhe($link) {

    $share = "javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=500');return false;";
    $compartilhe  = '<ul class="share-list">';

    $compartilhe .= '<li class="share-list__item">
        <a href="https://twitter.com/intent/tweet?text='.get_the_title().'&url='.$link.'" onclick="'.$share.'">
            <i class="icone fa fa-twitter"></i>
        </a>
    </li>';

    $compartilhe .= '<li class="share-list__item">
        <a class="btn" href="https://www.facebook.com/sharer/sharer.php?u='.$link.'" onclick="'.$share.'">
            <i class="icone fa fa-facebook"></i>
        </a>
    </li>';

    $compartilhe .= '<li class="share-list__item">
        <a href="#sharemail_modal" class="open-popup-reserva">
            <i class="fa fa-envelope"></i>
        </a>
    </li>';

    if(wp_is_mobile()):
        $whatsapp = 'http://api.whatsapp.com/send?text='.$link;
    else:
        $whatsapp = 'http://web.whatsapp.com/send?text='.$link;
    endif;

    $compartilhe .= '<li class="share-list__item i-whatsapp">
        <a href="'.$whatsapp.'" target="_blank">
            <i class="icone fa fa-whatsapp"></i>
        </a>
    </li>';

    $compartilhe .= '</ul>';
    echo $compartilhe;
}
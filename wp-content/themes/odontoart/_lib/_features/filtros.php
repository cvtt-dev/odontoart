<?php
/*
* Filtros Posts functions
* Desenvolvedor: Nicholas Lima
* Email: nick.lima.wp@gmail.com
*/

//=========================================================================================
// FILTROS META KEY
// Baseado no script: "Plugin Name: Admin Filter BY Custom Fields" (http://en.bainternet.info) de autoria de Bainternet (http://en.bainternet.info)
//=========================================================================================

add_filter( 'parse_query', 'WPCustom_filter' );
function WPCustom_filter($query) {
    global $pagenow;
    if ( is_admin() && $pagenow=='edit.php' && isset($_GET['filtro']) && $_GET['filtro'] != '') {
        $query->query_vars['meta_key'] = $_GET['filtro'];
    if (isset($_GET['value']) && $_GET['value'] != '')
        //$query->query_vars['meta_value'] = $_GET['value'];
        $query->query_vars['meta_query'] = array(
        array(
            'value'   => $_GET['value'],
            'key'     => $_GET['filtro'],
            'compare' => 'LIKE'
        ));
    }
}

add_action( 'restrict_manage_posts', 'WPCustom_filter_restrict_manage_posts' );
function WPCustom_filter_restrict_manage_posts() {
    global $wpdb;
    global $post_type;
    $current = isset($_GET['filtro'])? $_GET['filtro']:'';
    $current_v = isset($_GET['value'])? $_GET['value']:'';

    if('agenda' == $post_type) :
    echo 'Cidade:<input style="margin-left: 5px;" type="TEXT" name="value" value="'.$current_v.'" /><input type="hidden" name="filtro" value="agenda_local" />';
    endif;
}


//=========================================================================================
// COLUNAS
//=========================================================================================

//ADD CLASS (LARGURA COLUNA)
add_action('admin_head', 'WPCustom_columns_css');
function WPCustom_columns_css() {
    $css = '<style>';
    $css .= '.column-data {width: 8%;}';
    $css .= '.column-ingressos {width: 6%;}';
    $css .= '</style>';
    echo $css;
}

// REMOVE DEFAULT CATEGORY COLUMN
add_filter('manage_agenda_posts_columns', 'WPCustom_remove_column');
function WPCustom_remove_column($defaults) {
    //to get defaults column names:
    //print_r($defaults);
    unset($defaults['comments']);
    return $defaults;
}

//CRIA A COLUNA
add_filter('manage_agenda_posts_columns', 'WPCustom_columns_head');
function WPCustom_columns_head($defaults) {
    $defaults['data'] = 'Data Evento';
    $defaults['ingressos'] = 'Ingressos';
    return $defaults;
}

//EXIBE A COLUNA
add_action('manage_agenda_posts_custom_column', 'WPCustom_columns_content', 10, 2);
function WPCustom_columns_content($column_name, $post_ID) {

    if($column_name == 'data') {
        $data = get_post_meta($post_ID, 'agenda_data', true);
        echo date('d M Y', strtotime($data));
    }

    if($column_name == 'ingressos') {
        $ingressos = get_post_meta($post_ID, 'agenda_ingresso', true);
        $ingressos_out = get_post_meta($post_ID, 'agenda_ingresso_out', true);
        // echo '<p style="text-align: center;">';
        echo ($ingressos ? '<a href="'.$ingressos.'" target="_blank">Visitar Site</a>' : '-');
        echo ($ingressos_out == 1 ? '</br><span style="color: red;">Esgotado</span>' : '');
        // echo '</p>';
    }

}

//POSICIONA AS COLUNAS
add_filter('manage_agenda_posts_columns', 'WPCustom_columns_order');
function WPCustom_columns_order($columns) {
    $coluna       = 'title';
    $coluna2       = 'date';
    $order_column = array();

    foreach($columns as $key => $value) {
        if ($key==$coluna){
            //Move as colunas para antes de $coluna
            $order_column['data'] = '';
        }
        if ($key==$coluna2){
            //Move as colunas para antes de $coluna
            $order_column['ingressos'] = '';
        }
        $order_column[$key] = $value;
    }
    return $order_column;
}

//ORDER POST ASC/DESC
add_filter( 'manage_edit-agenda_sortable_columns', 'WPCustom_columns_sortable');
function WPCustom_columns_sortable( $columns ) {
    $columns['data'] = 'agenda_data';
    $columns['ingressos'] = 'agenda_ingresso_out';
    return $columns;
}

add_action( 'pre_get_posts', 'WPCustom_columns_orderby', 9 );
function WPCustom_columns_orderby( $query ) {
    global $post_type, $pagenow;
    if($pagenow == 'edit.php' && $post_type == 'agenda'){

        $orderby = $query->get('orderby');

        if('agenda_data' == $orderby) {
            $query->set('meta_key','agenda_data');
            $query->set('orderby','meta_value');
        }
    }
}
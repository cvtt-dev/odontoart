<?php
/*
* Data Excursões
* Desenvolvedor: Nicholas Lima
* Email: nick.lima.wp@gmail.com
*/

function dataexcursao($data){
    $meses = array(
        "01" => "Jan",
        "02" => "Fev",
        "03" => "Mar",
        "04" => "Abr",
        "05" => "Mai",
        "06" => "Jun",
        "07" => "Jul",
        "08" => "Ago",
        "09" => "Set",
        "10" => "Out",
        "11" => "Nov",
        "12" => "Dez"
    );

    $dia    = date('d', strtotime($data));
    $mes    = date('m', strtotime($data));
    echo $dia.'/'.$meses[$mes];
}

function diaexcursao($data){
    $dias = array(
        "0" => "Domingo",
        "1" => "Segunda",
        "2" => "Terça",
        "3" => "Quarta",
        "4" => "Quinta",
        "5" => "Sexta",
        "6" => "Sábado"
    );

    $semana = date('w', strtotime($data));
    echo $dias[$semana];
}

function removeAcentos($string){
    $text = strtr(utf8_decode($string), utf8_decode('àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ'), 'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
    $text = strtolower($text);
    echo $text;
}
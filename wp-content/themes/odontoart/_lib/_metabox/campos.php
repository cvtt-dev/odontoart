<?php
/*
* Metabox Settings
* Desenvolvedor: Nicholas Lima
* Email: nick.lima.wp@gmail.com
*/

//=========================================================================================
// START DEFINITION OF META BOXES
//=========================================================================================

add_filter( 'rwmb_meta_boxes', 'wpcf_meta_boxes' );
function wpcf_meta_boxes($meta_boxes) {

//=========================================================================================
// BLOG
//=========================================================================================

$meta_boxes[] = array(
    'id'        => 'blog_destaque',
    'title'     => 'Destacar Artigo',
    'pages'     => array('post'),
    'context'   => 'side',
    'priority'  => 'high',
    'fields'    => array(
        array(
            'name'       => '',
            'id'         => 'blog_destaque',
            'desc'       => 'Destacar o Artigo',
            'type'       => 'checkbox',
            'admin_columns' => array('position' => 'after title', 'title' => 'Destacado','sort' => true),
        ),
    )
);

//=========================================================================================
// EXCURSÕES
//=========================================================================================

$meta_boxes[] = array(

    'id' => 'excursoes_preco',
    'title' => 'Preços',
    'pages' => array( 'excursoes' ),
    'context' => 'side',
    'priority' => 'high',

    // List of meta fields
    'fields' => array(
        array(
            'name'          => 'Qtd de Parcelas',
            'id'            => 'excursoes_parcela',
            'type'          => 'slider',
            'suffix'        => ' x',
            'std'           => '12',
            'js_options'    => array(
                'min'       => 1,
                'max'       => 24,
                'step'      => 1,
            ),
            'required'      => true,
            'admin_columns' => array('position' => 'after title', 'title' => 'Parcelas','sort' => true),
        ),
        array(
            'name'          => 'Moeda',
            'id'            => 'excursoes_moeda',
            'type'          => 'text',
            'std'           => 'R$',
            'required'      => true,
            'admin_columns' => array('position' => 'after title', 'title' => 'Moeda'),
        ),
        array(
            'name'       => '',
            'id'         => 'excursoes_destaque',
            'desc'       => 'Destacar a Excursão',
            'type'       => 'checkbox',
            'admin_columns' => array('position' => 'after title', 'title' => 'Destacado','sort' => true),
        ),
    )
);

$meta_boxes[] = array(
    'id' => 'excursoes_datas',
    'title' => 'Detalhes da Excursão',
    'pages' => array( 'excursoes' ),
    'context' => 'normal',
    'priority' => 'high',

    // List of meta fields
    'fields' => array(
        array(
            'name'          => 'Data Saída',
            'id'            => 'excursoes_saida',
            'desc'          => 'Defina a data de Saída',
            'type'          => 'date',
            'format'        => 'yy-mm-dd',
            'required'      => true,
            'admin_columns' => array('position' => 'after title', 'title' => 'Saída', 'sort' => true),
        ),
        array(
            'name'          => 'Data Chegada',
            'id'            => 'excursoes_chegada',
            'desc'          => 'Defina a data de Chegada',
            'type'          => 'date',
            'format'        => 'yy-mm-dd',
            'required'      => true,
            'admin_columns' => array('position' => 'after excursoes_saida', 'title' => 'Chegada', 'sort' => true),
        ),
        array(
            'name'          => 'Embarque',
            'id'            => 'excursoes_embarque',
            'type'          => 'text',
            'required'      => true,
            'std'           => 'Rio de Janeiro, RJ',
            'desc'          => 'Defina a cidade do embarque. Ex: Rio de Janeiro, RJ',
            'admin_columns' => array('position' => 'after title', 'title' => 'Emabarque'),
        ),
        array(
            'name'          => 'Total de Noites',
            'id'            => 'excursoes_noites',
            'type'          => 'slider',
            'js_options'    => array(
                'min'       => 1,
                'max'       => 31,
                'step'      => 1,
            ),
            'required'      => true,
            'admin_columns' => array('position' => 'after title', 'title' => 'Nº Noites', 'sort' => true),
        ),
        array(
            'id'            => 'excursoes_condicoes',
            'type'          => 'file_advanced',
            'name'          => 'Condições Gerais',
            'desc'          => 'Arquivo em PDF',
            'mime_type'     => 'application/pdf',
            'max_file_uploads' => 1,
            'admin_columns' => array('position' => 'after excursoes_parcela', 'title' => 'Arquivo', 'sort' => true),
        ),
        array(
            'name'          => 'Ano',
            'id'            => 'agenda_ano',
            'type'          => 'hidden',
        ),
        array(
            'name'          => 'Mês',
            'id'            => 'agenda_mes',
            'type'          => 'hidden',
        ),
    )
);

$meta_boxes[] = array(
    'id'        => 'excursoes_pacotes',
    'title'     => 'Pacotes Disponíveis',
    'pages'     => array('excursoes'),
    'context'   => 'normal',
    'priority'  => 'high',
    'fields'    => array(
        array(
            'name'       => '',
            'id'         => 'excursoes_pacotes',
            'type'       => 'group',
            'clone'      => true,
            'sort_clone' => true,
            'fields'     => array(
                array(
                    'name'      => '',
                    'id'        => 'desc',
                    'type'      => 'text',
                    'desc'      => 'Descrição. Ex: Adulto em quarto duplo',
                ),
                array(
                    'name'      => '',
                    'id'        => 'valor',
                    'type'      => 'text',
                    'class'     => 'valor_excursao',
                    'desc'      => 'Valor da parcela. Ex: 333,00',
                ),
            ),
        ),
    )
);

$meta_boxes[] = array(
    'id'        => 'excursoes_detalhes',
    'title'     => 'Informações do Intinerário',
    'pages'     => array('excursoes'),
    'context'   => 'normal',
    'priority'  => 'high',
    'fields'    => array(
        array(
            'name'       => '',
            'id'         => 'excursoes_viagem',
            'type'       => 'group',
            'clone'      => true,
            'sort_clone' => false,
            'required'   => true,
            'fields'     => array(
                array(
                    'name'      => '',
                    'id'        => 'titulo',
                    'type'      => 'text',
                    'desc'      => 'Local. Ex: Rio de Janeiro/Fortaleza',
                ),
                array(
                    'name'      => '',
                    'id'        => 'descricao',
                    'type'      => 'wysiwyg',
                    'desc'      => 'Descrição das atividades',
                    'options' => array(
                        'textarea_rows' => 7,
                        'teeny'         => true,
                        'media_buttons' => false,
                    ),
                ),
            ),
        ),
    )
);

$meta_boxes[] = array(
    'id'        => 'excursoes_incluso',
    'title'     => 'Incluso no Pacote',
    'pages'     => array('excursoes'),
    'context'   => 'normal',
    'priority'  => 'high',
    'fields'    => array(
        array(
            'name'       => '',
            'id'         => 'excursoes_incluso',
            'type'       => 'text',
            'clone'      => true,
            'sort_clone' => true,
            'required'   => true,
        ),
    )
);

$meta_boxes[] = array(
    'id'        => 'excursoes_nincluso',
    'title'     => 'Não Incluso no Pacote',
    'pages'     => array('excursoes'),
    'context'   => 'normal',
    'priority'  => 'high',
    'fields'    => array(
        array(
            'name'       => '',
            'id'         => 'excursoes_nincluso',
            'type'       => 'text',
            'clone'      => true,
            'sort_clone' => true,
            'required'   => true,
        ),
    )
);

$meta_boxes[] = array(
    'id'        => 'excursoes_galeria',
    'title'     => 'Galeria',
    'pages'     => array('excursoes'),
    'context'   => 'normal',
    'priority'  => 'high',
    'fields'    => array(
        array(
            'name'             => '',
            'id'               => 'excursoes_galeria',
            'type'             => 'file_advanced',
            'max_file_uploads' => 20,
            'mime_type'    => 'image'
        ),
    )
);


//=========================================================================================
// DEPOIMENTOS
//=========================================================================================

$meta_boxes[] = array(
    'id'        => 'infos_quotes',
    'title'     => 'Detalhes',
    'pages'     => array('depoimentos'),
    'context'   => 'normal',
    'priority'  => 'high',
    'fields'    => array(
        array(
            'name'             => 'Cargo',
            'id'               => 'depoimentos_cargo',
            'type'             => 'text',
        ),
    )
);

//=========================================================================================
// SLIDES
//=========================================================================================

$meta_boxes[] = array(
    'id' => 'slide',
    'title' => 'Informações adicionais',
    'pages' => array( 'slides' ),
    'context' => 'normal',
    'priority' => 'high',

    // List of meta fields
    'fields' => array(
        array(
            'name'             => 'Icone',
            'id'               => 'icone_slide',
            'type'             => 'image_advanced',
            'max_file_uploads' => 1,
        ),

        array(
            'name'             => 'Subtítulo',
            'id'               => 'slide_subtitle',
            'type'             => 'text',
        ),

        array(
            'type'             => 'divider',
        ),

        array(
            'name'             => 'Título do botão',
            'id'               => 'slide_tit_btn',
            'type'             => 'text',
        ),

        array(
            'name'             => 'URL do botão',
            'id'               => 'slide_url',
            'type'             => 'url',
        ),
    )
);


//=========================================================================================
// REDES GALERIA
//=========================================================================================

$meta_boxes[] = array(
    'id' => 'redes',
    'title' => 'Galeria Redes',
    'post_types' => array('redes'),
    'context' => 'normal',
    'priority' => 'high',

    // List of meta fields
    'fields' => array(
        array(
            'id'               => 'redes_galeria',
            'name'             => 'Imgagens para galeria',
            'type'             => 'image_advanced',
            'max_file_uploads' => 10,
        ),

        array(
            'id'               => 'redes_referencia',
            'name'             => 'Ponto de referência',
            'type'             => 'text',
        ),

        array(
            'id'               => 'lat_long_emp',
            'name'             => 'Coordenadas',
            'type'             => 'text',
        ),
    )
);



//=========================================================================================
// END DEFINITION OF META BOXES
//=========================================================================================
    return $meta_boxes;
}
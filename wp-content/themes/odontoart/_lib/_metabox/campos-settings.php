<?php
/*
* Metabox Settings
* Desenvolvedor: Nicholas Lima
* Email: nick.lima.wp@gmail.com
*/

//=========================================================================================
// CREATE PAGE
//=========================================================================================

add_filter( 'mb_settings_pages', 'prefix_options_page' );
function prefix_options_page($settings_pages){
    $settings_pages[] = array(
        'id'          => 'theme-options',
        'option_name' => 'options_gerais',
        'menu_title'  => 'Opções do Tema',
        // 'parent'      => 'themes.php',
    );
    return $settings_pages;
}

//=========================================================================================
// START DEFINITION OF META BOXES
//=========================================================================================

add_filter( 'rwmb_meta_boxes', 'prefix_options_meta_boxes' );
function prefix_options_meta_boxes( $meta_boxes ) {

//=========================================================================================
// GTM
//=========================================================================================

$meta_boxes[] = array(
    'id'        => 'gtm_codes',
    'title'     => 'GTM Code',
    'context'   => 'side',
    'priority'  => 'high',
    'settings_pages' => 'theme-options',
    'fields'    => array(
     array(
        'name'             => '',
        'id'               => 'gtm_head',
        'desc'             => 'Code Head',
        'type'             => 'textarea',
    ),
     array(
        'name'             => '',
        'id'               => 'gtm_body',
        'desc'             => 'Code Body',
        'type'             => 'textarea',
    ),
 )
);

$meta_boxes[] = array(
    'id'        => 'links_imp',
    'title'     => 'Links Importantes',
    'context'   => 'side',
    'priority'  => 'high',
    'settings_pages' => 'theme-options',
    'fields'    => array(
     array(
        'name'             => 'Cliente',
        'id'               => 'links_imp_cliente',
        'type'             => 'url',
    ),
     array(
        'name'             => 'Dentista',
        'id'               => 'links_imp_dentista',
        'type'             => 'url',
    ),
 )
);


//=========================================================================================
// ENDERECOS
//=========================================================================================

$meta_boxes[] = array(
    'id'        => 'enderecos',
    'title'     => 'Endereços e Contatos',
    'context'   => 'normal',
    'priority'  => 'high',
    'settings_pages' => 'theme-options',
    'fields'    => array(
     array(
        'name'          => 'Endereço',
        'id'            => 'contato_endereco',
        'desc'          => 'Endereço',
        'type'          => 'wysiwyg',
        'required'      => true,
        'options'       => array(
            'textarea_rows' => 7,
            'teeny'         => true,
            'media_buttons' => false,
        ),
    ),
    array(
        'name'       => 'E-mail para Contato',
        'id'         => 'contato_email',
        'type'       => 'text',
        'required'   => true
    ),
    array(
        'name'       => 'Telefones',
        'id'         => 'contato_telefones',
        'type'       => 'text'
    ),
 )
);

//=========================================================================================
// CONTATO IMPORTANTES
//=========================================================================================

$meta_boxes[] = array(
    'id'        => 'contatos_importante',
    'title'     => 'Contatos importante',
    'context'   => 'normal',
    'priority'  => 'high',
    'settings_pages' => 'theme-options',
    'fields'    => array(

        array(
            'id'          => 'group_telefone_contatos',
            'name'        => 'Telefones',
            'type'        => 'group',
            'clone'       => true,
            'sort_clone'  => true,
            'collapsible' => true,
            'group_title' => array( 'field' => 'titulo' ),
            'save_state' => true,
            'max_clone'=> 4,
            
            'fields' => array(
                array(
                    'name' => 'Título',
                    'id'   => 'titulo',
                    'type' => 'text',
                ),

                array(
                    'name' => 'Telefone',
                    'id'   => 'telefone',
                    'type' => 'text',
                ),
            )
        )

    )
);

//=========================================================================================
// Central de vendas/ Atendimento
//=========================================================================================

$meta_boxes[] = array(
    'id'        => 'central_de_vendas',
    'title'     => 'Central de vendas / Atendimento',
    'context'   => 'normal',
    'priority'  => 'high',
    'settings_pages' => 'theme-options',
    'fields'    => array(    
        array(
            'id'          => 'group_telefone_vendas',
            'name'        => 'Telefones',
            'type'        => 'group',
            'clone'       => true,
            'sort_clone'  => true,
            'collapsible' => true,
            'group_title' => array( 'field' => 'titulo' ),
            'save_state' => true,
            'fields' => array(
                array(
                    'name' => 'Título',
                    'id'   => 'titulo',
                    'type' => 'text',
                ),

                array(
                    'name' => 'Telefone',
                    'id'   => 'telefone',
                    'type' => 'text',
                ),
            )
        )

    )
);

$meta_boxes[] = array(
    'id'        => 'conheca_planos',
    'title'     => 'Conheça nossos planos',
    'context'   => 'normal',
    'priority'  => 'high',
    'settings_pages' => 'theme-options',
    'fields'    => array(
        array(
            'id'          => 'planos_group',
            'name'        => 'Planos',
            'type'        => 'group',
            'clone'       => true,
            'sort_clone'  => true,
            'collapsible' => true,
            'group_title' => array( 'field' => 'titulo' ),
            'save_state' => true,
            'fields' => array(
                array(
                    'name' => 'Título',
                    'id'   => 'titulo',
                    'type' => 'text',
                ),

                array(
                    'name' => 'Subtítulo',
                    'id'   => 'subtitulo',
                    'type' => 'text',
                ),

                array(
                    'name' => 'Observação',
                    'id'   => 'obs',
                    'type' => 'text',
                ),

                array(
                    'name'             => 'Vantanges',
                    'id'               => 'text',
                    'type'             => 'text',
                    'clone'            => true
                ),
            )
        ),
    )
);


//=========================================================================================
// Central de vendas/ Atendimento
//=========================================================================================

$meta_boxes[] = array(
    'id'        => 'informacoes_app',
    'title'     => 'Informações sobre o APP',
    'context'   => 'normal',
    'priority'  => 'high',
    'settings_pages' => 'theme-options',
    'fields'    => array(
        array(
            'name' => 'Título',
            'id'   => 'app_titulo',
            'type' => 'text',
        ),

        array(
            'name' => 'Descrição',
            'id'   => 'app_descricao',
            'type' => 'textarea',
        ),

        array(
            'id'          => 'app_group_images',
            'name'        => 'Links para download',
            'type'        => 'group',
            'clone'       => true,
            'sort_clone'  => true,
            'collapsible' => true,
            'group_title' => array( 'field' => 'titulo' ),
            'save_state' => true,
            'fields' => array(
                array(
                    'name' => 'Título da loja',
                    'id'   => 'titulo',
                    'type' => 'text',
                ),

                array(
                    'name'             => 'Logo da loja',
                    'id'               => 'image',
                    'type'             => 'image_advanced',
                    'max_file_uploads' => 1,
                ),               
                
                array(
                    'name' => 'Link da loja',
                    'id'   => 'link',
                    'type' => 'text',
                ),
            )
        ),
    )
);


//=========================================================================================
// LINKS
//=========================================================================================

$meta_boxes[] = array(
    'id'             => 'settings_social',
    'title'          => 'Redes Sociais',
    'context'        => 'side',
    'priority'       => 'high',
    'settings_pages' => 'theme-options',
    'fields'         => array(
        array(
            'name' => 'URL Facebook',
            'id'   => 'settings_facebook_url',
            'type' => 'url',
        ),
        array(
            'name' => 'URL Instagram',
            'id'   => 'settings_instagram_url',
            'type' => 'url',
        ),
        array(
            'name' => 'URL Youtube',
            'id'   => 'settings_youtube_url',
            'type' => 'url',
        ),
        array(
            'name' => 'URL Linkedin',
            'id'   => 'settings_linkedin_url',
            'type' => 'url',
        ),
    ),
);


//=========================================================================================
// CRÉDITO
//=========================================================================================

$meta_boxes[] = array(
    'id'        => 'infos_cred',
    'title'     => 'Informações rodapé créditos',
    'context'   => 'side',
    'priority'  => 'high',
    'settings_pages' => 'theme-options',
    'fields'    => array(
        array(
            'name'             => 'Crédito Título',
            'id'               => 'credito_titulo',
            'type'             => 'textarea',
        ),

        array(
            'name'             => 'Logos laterais',
            'id'               => 'images',
            'type'             => 'image_advanced',
            'max_file_uploads' => 2,
        ),
    )
);

//=========================================================================================
// END DEFINITION OF META BOXES
//=========================================================================================
    return $meta_boxes;
}
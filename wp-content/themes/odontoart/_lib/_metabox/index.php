<?php
/*
* Metabox Settings
* Desenvolvedor: Nicholas Lima
* Email: nick.lima.wp@gmail.com
*/

if (is_admin()):
  define('RWMBC_URL', trailingslashit( get_stylesheet_directory_uri() . '/_lib/_metabox'));
  define('RWMBC_DIR', trailingslashit( STYLESHEETPATH . '/_lib/_metabox'));
  require_once RWMBC_DIR . 'campos.php';
  require_once RWMBC_DIR . 'campos-pages.php';
  require_once RWMBC_DIR . 'campos-taxonomies.php';
  require_once RWMBC_DIR . 'campos-settings.php';
  require_once RWMBC_DIR . 'campos-functions.php';
endif;
 ?>
<?php

//=========================================================================================
// START DEFINITION OF META BOXES
//=========================================================================================

add_filter( 'rwmb_meta_boxes', 'wpcf_meta_boxes_tax' );
function wpcf_meta_boxes_tax($meta_boxes) {

//=========================================================================================
// BLOG
//=========================================================================================

$meta_boxes[] = array(
    'title'      => '',
    'taxonomies' => 'category', // list of taxonomies. array or string
    'fields' => array(
         array(
            'name'             => 'Imagem Destacada',
            'id'               => 'cor_categoria',
            'type'             => 'color',
        ),
    ),
);

//=========================================================================================
// VIAGENS
//=========================================================================================

$meta_boxes[] = array(
    'title'      => '',
    'taxonomies' => 'viagens', // List of taxonomies. Array or string
    'fields' => array(
         array(
            'name'             => 'Imagem Destacada',
            'id'               => 'img_categoria',
            'type'             => 'file_advanced',
            'max_file_uploads' => 1,
            'desc'             => 'Capa da Categoria',
            'mime_type'        => 'image', // Leave blank for all file types
        ),
         array(
            'name'             => 'Exibir na Home',
            'id'               => 'destaque_categoria',
            'type'             => 'checkbox',
            'desc'             => 'Exibir na home',
        ),
    ),
);

//=========================================================================================
// END DEFINITION OF META BOXES
//=========================================================================================
    return $meta_boxes;
}
<?php
/*
* Metabox Settings
*/

//=========================================================================================
// START DEFINITION OF META BOXES
//=========================================================================================

add_filter( 'rwmb_meta_boxes', 'wpcf_meta_boxes_pages' );
function wpcf_meta_boxes_pages($meta_boxes) {

//=========================================================================================
//  INFORMACOES GERAIS
//=========================================================================================

$meta_boxes[] = array(
    'id' => 'pages_infos',
    'title' => 'Informações gerais',
    'pages' => array('page'),
    'context' => 'normal',
    'priority' => 'high',

    'include' => array(
        'relation'  => 'OR',
        'template'  => array('page-planos-empresa.php', 'page-planos-familia.php'),
    ),

    'fields' => array(
        array(
            'name'    => 'Cor do tema',
            'id'      => 'page_template_color',
            'type'    => 'radio',
            
            'options' => array(
                'green' => 'Verde',
                'orange' => 'Laranja',
                'purple' => 'Roxo',                
            ),
        ),

        array(
            'name'     => 'Image do card',
            'id'       => 'page_miniatura_image',
            'type'     => 'image_advanced',
            'max_file_uploads' => 1,
        ),
    )
);

//=========================================================================================
// CAPA
//=========================================================================================

$meta_boxes[] = array(
    'id' => 'pages_capa',
    'title' => 'Informações de capa',
    'pages' => array('page'),
    'context' => 'normal',
    'priority' => 'high',

    'include' => array(
        'relation'  => 'OR',
        'template'  => array('page-planos-empresa.php', 'page-planos-familia.php'),
    ),

    'fields' => array(

        array(
            'name'     => 'Image da capa',
            'id'       => 'page_capa_image',
            'type'     => 'image_advanced',
            'max_file_uploads' => 1,
        ),

        array(
            'name'     => 'Primeira parte',
            'id'       => 'page_capa_titulo_primeira',
            'type'     => 'text',
        ),

        array(
            'name'     => 'Segunda parte',
            'id'       => 'page_capa_titulo_segunda',
            'type'     => 'text',
        ),

        array(
            'name'     => 'Mini descrição',
            'id'       => 'page_capa_mini_descricao',
            'type'     => 'textarea',
        ),

        array(
            'name'     => 'Shortcode formulário',
            'id'       => 'page_capa_formulario',
            'type'     => 'text',
        ),

    )
);


//==============================================================================
// VANTAGENS EMPRESA
//==============================================================================

$meta_boxes[] = array(
    'id'        => 'page_vantagens_empresa',
    'title'     => 'Vantagens para empresa',
    'pages'     => array('page'),
    'context'   => 'normal',
    'priority'  => 'high',

    'include' => array(
        'relation'  => 'OR',
        'template'  => array('page-planos-empresa.php'),
    ),
    
    'fields'    => array(

        array(
            'name'     => 'Título 1ª parte',
            'id'       => 'page_vantagens_empresa_titulo_primeira',
            'type'     => 'text',
        ),

        array(
            'name'     => 'Título 2ª parte',
            'id'       => 'page_vantagens_empresa_titulo_segunda',
            'type'     => 'text',
        ),

        array(
            'name'      => 'Descrição',
            'id'        => 'page_vantagens_empresa_descricao',
            'type'      => 'textarea',
        ),

        array(
            'name'       => 'Ícones',
            'id'         => 'cards_vantagem_empresa',
            'type'       => 'group',
            'clone'       => true,
            'sort_clone'  => true,
            'collapsible' => true,
            'group_title' => array( 'field' => 'titulo' ),

            'fields'     => array(
                array(
                    'name'     => 'Ícone',
                    'id'       => 'icone',
                    'type'     => 'image_advanced',
                    'max_file_uploads' => 1,
                ),

                array(
                    'name'      => 'Título',
                    'id'        => 'titulo',
                    'type'      => 'text',
                ),
            ),
        ),
    )
);


//==============================================================================
// VANTAGENS
//==============================================================================

$meta_boxes[] = array(
    'id'        => 'page_vantagens',
    'title'     => 'Vantagens',
    'pages'     => array('page'),
    'context'   => 'normal',
    'priority'  => 'high',

    'include' => array(
        'relation'  => 'OR',
        'template'  => array('page-planos-empresa.php', 'page-planos-familia.php'),
    ),
    
    'fields'    => array(

        array(
            'name'     => 'Título 1ª parte',
            'id'       => 'page_vantagem_titulo_primeira',
            'type'     => 'text',
        ),

        array(
            'name'     => 'Título 2ª parte',
            'id'       => 'page_vantagem_titulo_segunda',
            'type'     => 'text',
        ),

        array(
            'name'      => 'Descrição',
            'id'        => 'page_vantagem_descricao',
            'type'      => 'textarea',
        ),

        array(
            'name'       => 'Ícones',
            'id'         => 'cards_vantagem',
            'type'       => 'group',
            'clone'       => true,
            'sort_clone'  => true,
            'collapsible' => true,
            'group_title' => array( 'field' => 'titulo' ),

            'fields'     => array(
                array(
                    'name'     => 'Ícone',
                    'id'       => 'icone',
                    'type'     => 'image_advanced',
                    'max_file_uploads' => 1,
                ),

                array(
                    'name'      => 'Título',
                    'id'        => 'titulo',
                    'type'      => 'text',
                ),
            ),
        ),
    )
);


$meta_boxes[] = array(
    'id'        => 'page_servicos',
    'title'     => 'Serviços',
    'pages'     => array('page'),
    'context'   => 'normal',
    'priority'  => 'high',

    'include' => array(
        'relation'  => 'OR',
        'template'  => array('page-planos-familia.php'),
    ),
    
    'fields'    => array(

        array(
            'name'      => 'Serviço',
            'id'        => 'page_servico',
            'type'      => 'text',
            'clone'     => true
        ),

    )
);


//==============================================================================
// CLIENTES
//==============================================================================

$meta_boxes[] = array(
    'id'        => 'page_cliente',
    'title'     => 'Clientes',
    'pages'     => array('page'),
    'context'   => 'normal',
    'priority'  => 'high',

    'include' => array(
        'relation'  => 'OR',
        'template'  => array('page-planos-empresa.php'),
    ),
    
    'fields'    => array(
        array(
            'name'       => 'Infos',
            'id'         => 'clientes',
            'type'       => 'group',
            'clone'       => true,
            'sort_clone'  => true,

            'fields'     => array(
                array(
                    'name'     => 'Logo do cliente',
                    'id'       => 'icone',
                    'type'     => 'image_advanced',
                    'max_file_uploads' => 1,
                ),

                array(
                    'name'      => 'URL do link',
                    'id'        => 'link',
                    'type'      => 'text',
                ),
            ),
        ),
    )
);

//==============================================================================
// CTA: ESPECIALISTA
//==============================================================================

$meta_boxes[] = array(
    'id' => 'pages_contatos',
    'title' => 'Configuração CTA especialista',
    'pages' => array('page'),
    'context' => 'normal',
    'priority' => 'high',

    'include' => array(
        'relation'  => 'OR',
        'template'  => array('page-sobre-nos.php', 'page-planos-empresa.php', 'page-planos-familia.php'),
    ),

    'fields' => array(
        array(
            'name'     => 'Primeira parte',
            'id'       => 'page_titulo_primeira',
            'type'     => 'text',
        ),

        array(
            'name'     => 'Segunda parte',
            'id'       => 'page_titulo_segunda',
            'type'     => 'text',
        ),

        array(
            'name'     => 'Mini descrição',
            'id'       => 'page_mini_descricao',
            'type'     => 'textarea',
        ),

        array(
            'name'     => 'Imagem para home',
            'id'       => 'page_image_home',
            'type'     => 'image_advanced',
            'max_file_uploads' => 1,
        ),

         
    )
);

$meta_boxes[] = array(
    'id' => 'pages_cabecalho',
    'title' => 'Configurações de cabeçalho',
    'pages' => array('page'),
    'context' => 'normal',
    'priority' => 'high',

    'include' => array(
        'relation'  => 'OR',
        'template'  => array('page-sobre-nos.php', 'page-rede-credenciadas.php'),
    ),

    'fields' => array(
        array(
            'name'     => 'Imagem personalizada',
            'id'       => 'page_cabecalho_img',
            'type'     => 'image_advanced',
            'max_file_uploads' => 1,
        ),

        array(
            'name'     => 'Primeira parte',
            'id'       => 'page_cabecalho_tit_primeira',
            'type'     => 'text',
        ),

        array(
            'name'     => 'Segunda parte',
            'id'       => 'page_cabecalho_tit_segunda',
            'type'     => 'text',
        ),
    )
);

//==============================================================================
// PAGE: REDE PROPRIA
//==============================================================================

// $meta_boxes[] = array(
//     'id' => 'pages_rede_propria',
//     'title' => 'Redes Própria',
//     'pages' => array('page'),
//     'context' => 'normal',
//     'priority' => 'high',

//     'include' => array(
//         'relation'  => 'OR',
//         'template'  => array('page-sobre-nos.php'),
//     ),

//     'fields' => array(
//         array(
//             'name'     => 'Primeira parte',
//             'id'       => 'page_rede_propria_tit_primeira',
//             'type'     => 'text',
//         ),

//         array(
//             'name'     => 'Segunda parte',
//             'id'       => 'page_rede_propria_tit_segunda',
//             'type'     => 'text',
//         ),

//         array(
//             'type' => 'divider'
//         ),

//         array(
//             'name'       => 'Ícones',
//             'id'         => 'cards_rede',
//             'type'       => 'group',
//             'clone'       => true,
//             'sort_clone'  => true,
//             'collapsible' => true,
//             'group_title' => array( 'field' => 'titulo' ),

//             'fields'     => array(

//                 array(
//                     'id'               => 'id_rede_galeria',
//                     'name'             => '',
//                     'type'             => 'file_advanced',
//                     'max_file_uploads' => 10,
//                     'mime_type'    => 'image'
//                 ),


//                 array(
//                     'name'      => 'Título',
//                     'id'        => 'titulo',
//                     'type'      => 'text',
//                 ),
                
//                 array(
//                     'name'      => 'Logradouro',
//                     'id'        => 'logradouro',
//                     'type'      => 'text',
//                 ),

//                 array(
//                     'name'      => 'Ponto de referência',
//                     'id'        => 'ponto_referencia',
//                     'type'      => 'text',
//                 ),
//             ),
//         ),
//     )
// );

//==============================================================================
// END DEFINITION OF META BOXES
//==============================================================================
    return $meta_boxes;
}
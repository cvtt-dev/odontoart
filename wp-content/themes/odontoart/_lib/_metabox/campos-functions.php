<?php
/*
* Metabox Settings
* Desenvolvedor: Nicholas Lima
* Email: nick.lima.wp@gmail.com
*/

function save_agenda_ap( $post_ID, $post, $update ) {

  $data  = get_post_meta( $post_ID, 'agenda_data', true);
    
  if(!$data) return false;

  $ano = date("Y", strtotime($data));
  $mes = date("m", strtotime($data));

  update_post_meta($post_ID, 'agenda_ano', $ano);
  update_post_meta($post_ID, 'agenda_mes', $mes);

}

add_action( 'save_post', 'save_agenda_ap', 10, 3 );
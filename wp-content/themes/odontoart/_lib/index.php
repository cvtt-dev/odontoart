<?php
/*
* Configurações do Tema
* Desenvolvedor: Nicholas Lima
* Email: nick.lima.wp@gmail.com
*/

//=========================================================================================
//================================ INCLUDE FUNCTIONS ======================================
//=========================================================================================

//===================================Config Tema===========================================
require_once locate_template('/_lib/_admin/dashboard.php');//................DASHBOARD TEMA
require_once locate_template('/_lib/_admin/admin.php');//....................STYLE LOGIN/ADMIN
require_once locate_template('/_lib/_admin/options.php');//..................OPÇÕES DO TEMA
//===================================Features==============================================
require_once locate_template('/_lib/_features/bem.php');//...................MENU BEM CSS
require_once locate_template('/_lib/_features/cforms.php');//................CFORMS
require_once locate_template('/_lib/_features/gtmcode.php');//...............GTM CODE
require_once locate_template('/_lib/_features/blog.php');//..................BLOG FUNCTIONS
require_once locate_template('/_lib/_features/excerpt.php');//...............EXCERPT FUNCTIONS
require_once locate_template('/_lib/_features/datas-excursoes.php');//.......DATAS EXCURSÕES
require_once locate_template('/_lib/_features/pagnav.php');//................PAGINATION
require_once locate_template('/_lib/_features/remove.php');//................CLEAN FUNCTIONS
require_once locate_template('/_lib/_features/links.php');//.................SOCIAL LINKS
require_once locate_template('/_lib/_features/share.php');//.................SHARE LINKS
//===================================Metabox===============================================
require_once locate_template('/_lib/_metabox/index.php');//..................POST TYPE FUNCTIONS
//===================================Wordpress===============================================
require_once locate_template('/_lib/_wp/posts.php');//.......................POST TYPE FUNCTIONS
require_once locate_template('/_lib/_wp/taxonomies.php');//..................TAXONOMIES FUNCTIONS
require_once locate_template('/_lib/_wp/thumbs.php');//......................THUMBNAIL FUNCTIONS
require_once locate_template('/_lib/_wp/scripts.php');//.....................SCRIPTS E CSS
require_once locate_template('/_lib/_wp/ajax.php');//........................FUNÇÕES AJAX

function getImage($img) {
  return get_template_directory_uri().'/assets/images/'.$img;
}


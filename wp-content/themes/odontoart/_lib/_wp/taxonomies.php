<?php
/*
* Configurações de Taxonomias
* Desenvolvedor: Nicholas Lima
* Email: nick.lima.wp@gmail.com
*/

//=========================================================================================
// TAXONOMIAS CIDADES
//=========================================================================================

// $labels = array( 'name' => _x( 'NOME', 'NOME EXIBIÇÃO', 'text_domain' ));
// $args   = array(
//   'labels'            => $labels,
//   'hierarchical'      => true,
//   'public'            => true,
//   'show_ui'           => true,
//   'show_admin_column' => true,
//   'show_in_nav_menus' => true,
//   'show_tagcloud'     => true,
//   'rewrite'           => array('slug' => 'SLUG'),
// );

// register_taxonomy( 'TAXONOMIA', array( 'POST TYPE ATRIBUIDOS' ), $args);

//=========================================================================================
// TAXONOMIAS EXCURSÕES
//=========================================================================================

$labels = array( 'name' => _x( 'Tipos', 'Tipos Perguntas', 'text_domain' ));
$args   = array(
  'labels'            => $labels,
  'hierarchical'      => true,
  'public'            => true,
  'show_ui'           => true,
  'show_admin_column' => true,
  'show_in_nav_menus' => true,
  'show_tagcloud'     => true,
  'rewrite'           => array('slug' => 'tipos'),
);

register_taxonomy( 'tipos', array( 'perguntas' ), $args);

//=========================================================================================
// FLUSH REWRITE
//=========================================================================================

function custom_taxonomy_flush_rewrite() {
    global $wp_rewrite;
    $wp_rewrite->flush_rules();
}
add_action('init', 'custom_taxonomy_flush_rewrite');
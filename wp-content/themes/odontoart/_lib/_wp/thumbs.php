<?php
/*
* Thumbnail functions
* Desenvolvedor: Nicholas Lima
* Email: nick.lima.wp@gmail.com
*/

//=========================================================================================
// ADICIONANDO TAMANHHO DE IMAGENS
//=========================================================================================

if (function_exists('add_image_size')) {
    add_image_size('thumb-840x380', 840, 380, true);
    add_image_size('thumb-530x185', 530, 185, true);
    add_image_size('thumb-420x220', 420, 220, true);
    add_image_size('thumb-400x400', 400, 400, true);
    add_image_size('thumb-170x170', 170, 170, true);
}

//=========================================================================================
// RESETA TAMANHHO DE IMAGENS
//=========================================================================================

// add_filter('intermediate_image_sizes','get_sizes');
// function get_sizes ($sizes){
//     $type = get_post_type($_REQUEST['post_id']);
//     foreach($sizes as $key => $value){
//         //Gera o tamanhho full para o slide
//         if($type=='slides'  &&  $value != 'full' && $value != 'thumb-385x180'){
//             unset($sizes[$key]);
//         }
//         if($type=='page'  &&  $value != 'full'){unset($sizes[$key]);} //Gera o tamanhho full para pages
//     }
//     return $sizes;
// }

//=========================================================================================
// LAZY LOAD FUNCTIONS
//=========================================================================================

function imglazy($post, $size, $efeito, $alt ) {
    $thumb = wp_get_attachment_image_src( $post, $size );
    $url = $thumb['0'];
    echo '<img class="b-lazy '.$efeito.'" data-src="'.$url.'" alt="'.$alt.'" title="'.$alt.'">';
    echo '<noscript><img src="'.$url.'" alt="'.$alt.'" title="'.$alt.'"></noscript>';
}

function thumblazy($post, $size, $efeito, $alt ) {
    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post), $size);
    $url = $thumb['0'];

    if ( has_post_thumbnail($post) ) :
        echo '<img class="b-lazy '.$efeito.'" data-src="'.$url.'" width="'.$thumb['1'].'" height="'.$thumb['2'].'" alt="'.$alt.'" title="'.$alt.'">';
        echo '<noscript><img src="'.$url.'" alt="'.$alt.'" width="'.$thumb['1'].'" height="'.$thumb['2'].'" title="'.$alt.'"></noscript>';
    else :
        echo '<img src="'.get_template_directory_uri().'/assets/images/no-thumbs.jpg" />';
    endif;
}

function lazyimage( $atts ) {
    // Attributes
    extract( shortcode_atts(
        array(
            'post'   => '',
            'size'   => '',
            'efeito' => 'fade',
            'alt'    => '',
            'tipo'   => 'thumb'
        ), $atts)
    );

    if(empty($post)) return false;
    if($tipo == 'attachment') {
        imglazy($post, $size, $efeito, $alt);
    } else {
        thumblazy($post, $size, $efeito, $alt);
    }
}
add_shortcode( 'lazy', 'lazyimage');

//=========================================================================================
// GET THUMB IMG
//=========================================================================================

function getThumbImg($size, $noimage){
    global $post;

    if(has_post_thumbnail($post)):
        the_post_thumbnail($size);
    else:
        echo '<img src="'.get_template_directory_uri().'/assets/images/no-image-'.$noimage.'.jpg" />';
    endif;
}
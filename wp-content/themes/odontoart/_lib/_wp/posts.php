<?php
/**
* Custom Post Types
* Desenvolvedor: Allef Bruno
*/

//=========================================================================================
// POST TYPE DEPOIMENTOS
//=========================================================================================

function post_type_perguntas_register() {
    $labels = array(
        'name' => 'perguntas',
        'singular_name' => 'Pergunta Frequente',
        'menu_name' => 'Perguntas Frequentes',
        'add_new' => _x('Adicionar Perguntas', 'item'),
        'add_new_item' => __('Adicionar Nova Pergunta'),
        'edit_item' => __('Editar Pergunta'),
        'new_item' => __('Nova Pergunta')
    );

    $args = array(
        'labels' => $labels,
        'public' => false,
        'publicly_queryable' => false,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'perguntas'),
        'capability_type' => 'post',
        'has_archive' => false,
        'hierarchical' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-megaphone',
        'supports' => array('title', 'editor')
    );
    register_post_type('perguntas', $args);
}
add_action('init', 'post_type_perguntas_register');

//=========================================================================================
// POST TYPE SLIDES
//=========================================================================================

function post_type_slide_register() {
    $labels = array(
        'name' => 'Slides',
        'singular_name' => 'Slide',
        'menu_name' => 'Slides',
        'add_new' => _x('Adicionar Slide', 'item'),
        'add_new_item' => __('Adicionar Novo Slide'),
        'edit_item' => __('Editar Slide'),
        'new_item' => __('Novo Slide')
    );

    $args = array(
        'labels' => $labels,
        'public' => false,
        'publicly_queryable' => false,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'slides'),
        'capability_type' => 'post',
        'has_archive' => false,
        'hierarchical' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-images-alt2',
        'supports' => array('title')
    );
    register_post_type('slides', $args);
}
add_action('init', 'post_type_slide_register');


//=========================================================================================
// POST TYPE REDES
//=========================================================================================

function post_type_rede_register() {
    $labels = array(
        'name' => 'Redes',
        'singular_name' => 'Rede',
        'menu_name' => 'Redes',
        'add_new' => _x('Adicionar Rede', 'item'),
        'add_new_item' => __('Adicionar Nova Rede'),
        'edit_item' => __('Editar Rede'),
        'new_item' => __('Nova Rede')
    );

    $args = array(
        'labels' => $labels,
        'public' => false,
        'publicly_queryable' => false,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'redes'),
        'capability_type' => 'post',
        'has_archive' => false,
        'hierarchical' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-networking',
        'supports' => array('title', 'editor')
    );
    register_post_type('redes', $args);
}
add_action('init', 'post_type_rede_register');
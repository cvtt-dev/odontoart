<?php 
    /* Template Name: Página Comparação */
    get_template_part('templates/html','header'); 

    global $post;
    $post_slug = $post->post_name;

    while (have_posts()) : the_post(); 
?>

<section class="odonto-page odonto-page--<?php echo $post_slug; ?>">
    <?php include_once locate_template('templates/sobre/capa.php') ; ?>

    <article class="odonto-article odonto-article--<?php echo $post_slug; ?> container">
      <div class="row">
        <div class="col-md-8">
        <?php the_content(); ?>
        </div>

        <div class="col-md-4">
          <?php get_template_part('templates/sidebar','planos'); ?>
        </div>
      </div>
    </article>
</section>

<?php
  endwhile; wp_reset_postdata(); 
  get_template_part('templates/html','footer'); 
?>
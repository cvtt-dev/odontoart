<?php 
    /* Template Name: Página Sobre nós */

    get_template_part('templates/html','header'); 
    
    while (have_posts()) : the_post(); 
?>

<section class="odonto-page odonto-page--sobre-nos">
    <!-- capa -->
    <?php include_once locate_template('templates/sobre/capa.php') ; ?>
    <!-- content -->
    <?php include_once locate_template('templates/sobre/content.php') ; ?>
    <!-- rede propria -->
    <?php include_once locate_template('templates/sobre/rede-propria.php') ; ?>    
    <!-- perfeito para voce -->
    <?php include_once locate_template('templates/sobre/perfeito-para-voce.php') ; ?>
    <!-- nossa rede credenciada -->
    <?php include_once locate_template('templates/sobre/rede-credenciada.php') ; ?>    
</section>

<?php endwhile; wp_reset_postdata(); ?>
<?php get_template_part('templates/html','footer'); ?>
<?php 
    /* Template Name: Página Contato */
    get_template_part('templates/html','header'); 

    global $post;
    $post_slug = $post->post_name;

    while (have_posts()) : the_post(); 
?>

<section class="odonto-page odonto-page--<?php echo $post_slug; ?>">
    <?php include_once locate_template('templates/sobre/capa.php') ; ?>

    <article class="odonto-article odonto-article--contato container">
      <div class="row">
        <div class="col-md-6">
          <h2 class="odonto-title odonto-title--color-green">Entre em contato conosco <strong>através do formulário abaixo:</strong></h2>

          <?php echo do_shortcode('[contact-form-7 id="214" title="PAGE - Contato"]'); ?>
        </div>

        <div class="col-md-6">
          
          <figure class="odonto-img-center">
            <img src="<?php echo getImage('/img-contato.png')?>" />
          </figure>

        </div>
      </div>
    </article>
</section>

<?php
  endwhile; wp_reset_postdata(); 
  get_template_part('templates/html','footer'); 
?>
<?php 
    /* Template Name: Página Imposto de Renda */
    get_template_part('templates/html','header'); 

    global $post;
    $post_slug = $post->post_name;

    while (have_posts()) : the_post(); 
?>

<section class="odonto-page odonto-page--<?php echo $post_slug; ?>">
    <?php include_once locate_template('templates/sobre/capa.php') ; ?>

    <article class="odonto-article odonto-article--contato container">
      <div class="row">
        <div class="col-md-6">
          <h2 class="odonto-title odonto-title--color-green">
            Insira seu CPF para obter seu 
            <strong>informe de imposto de renda:</strong>
          </h2>


          <form class="odonto-form odonto-form--gray" action="http://corporativo.odontoartonline.com.br/SYS/Associados/Acesso.aspx" method="get">
            <div class="form-row">
<input type="hidden" name="DeclaracaoIR" value="1">
                <input 
                  type="text" 
                  name="ID" id="segunda_via_boleto"
                  class="form-control" 
                  aria-required="true" 
                  aria-invalid="false" 
                  placeholder="CPF" 
                />
            </div>

            <div class="form-row">
              <input 
                type="submit" 
                value="Gerar" 
                class="odonto-btn odonto-btn--green odonto-btn--seta-white" 
              />
            </div>
          </form>
        </div>

      </div>
    </article>
</section>

<?php
  endwhile; wp_reset_postdata(); 
  get_template_part('templates/html','footer'); 
?>

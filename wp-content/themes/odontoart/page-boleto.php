<?php 
    /* Template Name: Página 2a via Boleto */
    get_template_part('templates/html','header'); 

    global $post;
    $post_slug = $post->post_name;

    while (have_posts()) : the_post(); 
?>

<section class="odonto-page odonto-page--<?php echo $post_slug; ?>">
    <?php include_once locate_template('templates/sobre/capa.php') ; ?>

    <article class="odonto-article odonto-article--contato container">
      <div class="row">
        <div class="col-md-8">
          <h2 class="odonto-title odonto-title--color-green">
            Insira seu CPF para 
            <strong>emitir sua 2ª Via:</strong>
          </h2>

          <form class="odonto-form odonto-form--gray" action="http://odontoart.com/internas/gerar-2-via.php" method="post">
            <div class="form-row">
                <input 
                  type="text" 
                  name="cpf" id="segunda_via_boleto"
                  class="form-control" 
                  aria-required="true" 
                  aria-invalid="false" 
                  placeholder="CPF" 
                />
            </div>

            <div class="form-row">
              <input 
                type="submit" 
                value="Gerar" 
                class="odonto-btn odonto-btn--green odonto-btn--seta-white" 
              />
            </div>
          </form>
        </div>

      </div>
    </article>
</section>

<?php
  endwhile; wp_reset_postdata(); 
  get_template_part('templates/html','footer'); 
?>

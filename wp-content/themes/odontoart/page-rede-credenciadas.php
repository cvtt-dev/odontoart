<?php 
    /* Template Name: Página Rede Credenciadas */
    get_template_part('templates/html','header'); 

    global $post;
    $post_slug = $post->post_name;

    while (have_posts()) : the_post(); 
?>

<section class="odonto-page odonto-page--<?php echo $post_slug; ?>">
    <?php include_once locate_template('templates/sobre/capa.php') ; ?>


<article class="odonto-article odonto-article--rede container">
<div class="row">
<iframe src="http://corporativo.odontoartonline.com.br/SYS/Rede_Atendimento/Rede_Atendimento.aspx?modal=1" width="100%" height="800" frameborder="0"></iframe>
</div>
</article>

<article class="odonto-article odonto-article--rede container" style="display:none;">
      <a href="#" class="odonto-btn" data-side-open>Abrir filtros</a>

      <div class="row">
        <aside class="col-md-4 side" data-side-container>
          <a href="#" class="side__close" data-side-close>
            <svg width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd"><path d="M2.117 12l7.527 6.235-.644.765-9-7.521 9-7.479.645.764-7.529 6.236h21.884v1h-21.883z"/></svg>
          </a>

          <h2 class="odonto-title odonto-title--color-black">Encotre seu <strong>especialista</strong></h2>

          <form class="odonto-form odonto-form--gray odonto-form--filter">
            <div class="form-row">
              <label>Estado <small>(obrigatório)</small></label>

              <div class="odonto-select odonto-select--green">
                <select name="estado">
                  <option>Ceará</option>
                  <option>Santa Catarina</option>
                </select>
              </div>
            </div>

            <div class="form-row">
              <label>Cidade <small>(obrigatório)</small></label>

              <div class="odonto-select odonto-select--green">
                <select name="estado">
                  <option>Fortaleza</option>
                  <option>Caucaia</option>
                </select>
              </div>
            </div>

            <div class="form-row">
              <input type="text" class="form-control" placeholder="Dentista/Clínica" />
            </div>

            <div class="form-row">
              <div class="odonto-select">
                <select name="estado">
                  <option>Todas</option>
                  <option>Plano 1</option>
                  <option>Plano 2</option>
                </select>
              </div>
            </div>

            <div class="form-row">
              <label>Especialidade</label>

              <div class="odonto-select">
                <select name="estado">
                  <option>Todas</option>
                  <option>Plano 1</option>
                  <option>Plano 2</option>
                </select>
              </div>
            </div>

            <div class="form-row">
              <label>Plano</label>

              <div class="odonto-select">
                <select name="estado">
                  <option>Todas</option>
                  <option>Plano 1</option>
                  <option>Plano 2</option>
                </select>
              </div>
            </div>

            <button class="odonto-btn odonto-btn--gren odonto-btn--seta-white">Pesquisar</button>
          </form>
        </aside>

        <div class="col-md-8">
          <h3 class="odonto-title odonto-title--color-black odonto-title--small">Encontre <strong>46</strong> especialista em Fortaleza, CE</h3>

          <div class="box-encontre">
            <div class="box-encontre__info">
              <h2>GLEYDSON LUCAS - ODONTOLOGIA ESPECIALIZADA</h2>

              <div class="box-encontre-row">
                <strong>Endereço:</strong>
                <span>RUA AMADEU FURTADO, 85 - SALA 2 E 3</span>
              </div>
            </div>

            <div class="box-encontre__telefone">
              <p><small>(85)</small> 3252.1227</p>
              <a href="#">Como chegar ></a>
            </div>

            <div class="box-encontre__action">
              <a href="#">Mais infos</a>
            </div>
          </div>

          <div class="box-encontre">
            <div class="box-encontre__info">
              <h2>GLEYDSON LUCAS - ODONTOLOGIA ESPECIALIZADA</h2>

              <div class="box-encontre-row">
                <strong>Endereço:</strong>
                <span>RUA AMADEU FURTADO, 85 - SALA 2 E 3</span>
              </div>
            </div>

            <div class="box-encontre__telefone">
              <p><small>(85)</small> 3252.1227</p>
              <a href="#">Como chegar ></a>
            </div>

            <div class="box-encontre__action">
              <a href="#">Mais infos</a>
            </div>
          </div>

          <div class="box-encontre">
            <div class="box-encontre__info">
              <h2>GLEYDSON LUCAS - ODONTOLOGIA ESPECIALIZADA</h2>

              <div class="box-encontre-row">
                <strong>Endereço:</strong>
                <span>RUA AMADEU FURTADO, 85 - SALA 2 E 3</span>
              </div>
            </div>

            <div class="box-encontre__telefone">
              <p><small>(85)</small> 3252.1227</p>
              <a href="#">Como chegar ></a>
            </div>

            <div class="box-encontre__action">
              <a href="#">Mais infos</a>
            </div>
          </div>

          <div class="box-encontre">
            <div class="box-encontre__info">
              <h2>GLEYDSON LUCAS - ODONTOLOGIA ESPECIALIZADA</h2>

              <div class="box-encontre-row">
                <strong>Endereço:</strong>
                <span>RUA AMADEU FURTADO, 85 - SALA 2 E 3</span>
              </div>
            </div>

            <div class="box-encontre__telefone">
              <p><small>(85)</small> 3252.1227</p>
              <a href="#">Como chegar ></a>
            </div>

            <div class="box-encontre__action">
              <a href="#">Mais infos</a>
            </div>
          </div>

          <div class="box-encontre">
            <div class="box-encontre__info">
              <h2>GLEYDSON LUCAS - ODONTOLOGIA ESPECIALIZADA</h2>

              <div class="box-encontre-row">
                <strong>Endereço:</strong>
                <span>RUA AMADEU FURTADO, 85 - SALA 2 E 3</span>
              </div>
            </div>

            <div class="box-encontre__telefone">
              <p><small>(85)</small> 3252.1227</p>
              <a href="#">Como chegar ></a>
            </div>

            <div class="box-encontre__action">
              <a href="#">Mais infos</a>
            </div>
          </div>

          <div class="box-encontre">
            <div class="box-encontre__info">
              <h2>GLEYDSON LUCAS - ODONTOLOGIA ESPECIALIZADA</h2>

              <div class="box-encontre-row">
                <strong>Endereço:</strong>
                <span>RUA AMADEU FURTADO, 85 - SALA 2 E 3</span>
              </div>
            </div>

            <div class="box-encontre__telefone">
              <p><small>(85)</small> 3252.1227</p>
              <a href="#">Como chegar ></a>
            </div>

            <div class="box-encontre__action">
              <a href="#">Mais infos</a>
            </div>
          </div>

          <div class="box-encontre">
            <div class="box-encontre__info">
              <h2>GLEYDSON LUCAS - ODONTOLOGIA ESPECIALIZADA</h2>

              <div class="box-encontre-row">
                <strong>Endereço:</strong>
                <span>RUA AMADEU FURTADO, 85 - SALA 2 E 3</span>
              </div>
            </div>

            <div class="box-encontre__telefone">
              <p><small>(85)</small> 3252.1227</p>
              <a href="#">Como chegar ></a>
            </div>

            <div class="box-encontre__action">
              <a href="#">Mais infos</a>
            </div>
          </div>

          <div class="box-encontre">
            <div class="box-encontre__info">
              <h2>GLEYDSON LUCAS - ODONTOLOGIA ESPECIALIZADA</h2>

              <div class="box-encontre-row">
                <strong>Endereço:</strong>
                <span>RUA AMADEU FURTADO, 85 - SALA 2 E 3</span>
              </div>
            </div>

            <div class="box-encontre__telefone">
              <p><small>(85)</small> 3252.1227</p>
              <a href="#">Como chegar ></a>
            </div>

            <div class="box-encontre__action">
              <a href="#">Mais infos</a>
            </div>
          </div>

        </div>
      </div>
    </article>
</section>

<?php
  endwhile; wp_reset_postdata(); 
  get_template_part('templates/html','footer'); 
?>

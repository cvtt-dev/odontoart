<?php 
  get_template_part('templates/html','header'); 
  include(locate_template('templates/blog-capa.php'));
  $exclude = array(); ?>

  <section class="odonto-article">
    <div class="container">
      <div class="row">
        <div class="col-md-9">
          <?php include(locate_template('templates/blog-archive.php')); ?>
        </div>

        <div class="col-md-3">
          <?php get_template_part('templates/sidebar','planos'); ?>
        </div>
      </div>
    </div>
  </section>

  <?php 
  get_template_part('templates/html','footer'); 
?>

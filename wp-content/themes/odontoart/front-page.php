<?php get_template_part('templates/html','header'); ?>
<?php get_template_part('templates/frontpage', 'slides'); ?>
<?php get_template_part('templates/frontpage', 'planos');?>
<?php get_template_part('templates/frontpage', 'sobre-nos');?>
<?php get_template_part('templates/frontpage', 'vantagens');?>
<?php get_template_part('templates/frontpage', 'encontre');?>
<?php get_template_part('templates/html', 'baixe-app');?>
<?php get_template_part('templates/frontpage', 'central');?>

<?php if (!wp_is_mobile()) : ?>
  <div class="odonto-apps-icons" id="alertApp">
    <div class="odonto-apps-icons__txt">
      <h2 class="title">Baixe agora nosso App</h2>  
      <div class="legend">
        <a class="link link--appstore" href="https://apps.apple.com/br/app/odontoart-associado/id1206858386?l=en">Apple Store</a>
        <a class="link link--playstore" href="https://play.google.com/store/apps/details?id=com.odontoart.associado">Google Play</a>
      </div>                  
    </div>
    <div id="close" class="close">x</div>
  </div>
<?php endif; ?>

<?php get_template_part('templates/html','footer'); ?>